"use strict";
const config = require('./config');
const app = require('./server/api');
const https = require('https');
const http = require('http');
const fs = require('fs');
const env = process.env.NODE_ENV || 'dev';
const CronJob = require('cron').CronJob;
const model = require('./server/models/v1');

const job = new CronJob('0 * */6 * * *', ()=> {
        model.events.destroy({where :{isDeleted: true}, cascade:true})
            .then((data)=>{
                console.log(`INFO: DELETED ${data} events | TIME: ${new Date()}`);
            })
            .catch((err)=>{
                console.log(`ERROR: ${err} | TIME: ${new Date()}`);
            });
    },
    null,
    true,
    null
);

console.log(job.running);

http.createServer(app).listen(config.server.port, serverStartCallback);

function serverStartCallback() {
    console.log("Express server is now live at port " + this.address().port + " with " + env + " config");
}

module.exports = app;