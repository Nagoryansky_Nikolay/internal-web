'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.sequelize.query(`
            DROP TRIGGER t_followers_count ON friendships;
            DROP FUNCTION  IF EXISTS  followers_count();
        `
        );
    },

    down: function (queryInterface, Sequelize) {
        // return queryInterface.sequelize.query();
    }
};