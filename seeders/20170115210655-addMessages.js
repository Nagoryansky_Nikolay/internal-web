'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.sequelize.query(`
            CREATE TABLE IF NOT EXISTS "messages" (
            "id"   SERIAL , 
            "text" TEXT, 
            "chatId" INTEGER REFERENCES "chats" ("id"), 
            "senderId" INTEGER, 
            "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL, 
            "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL, 
            PRIMARY KEY ("id"));`
        );
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('messages');
    }
};