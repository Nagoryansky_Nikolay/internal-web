'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.sequelize.query(`
            CREATE OR REPLACE FUNCTION followers_count() RETURNS TRIGGER AS $$
            DECLARE
            BEGIN
                UPDATE users SET followers_count=followers_count+1 WHERE id=new.friend_id;
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;  
            CREATE TRIGGER t_followers_count
            AFTER INSERT ON friendships FOR EACH ROW EXECUTE PROCEDURE followers_count();        
        `
        );
    },

    down: function (queryInterface, Sequelize) {
    }
};