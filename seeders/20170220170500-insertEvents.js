'use strict';
const USERS_COUNT = 29;
const EVENTS_COUNT = 20;
const LATITUDE = 48.470144;
const LONGITUDE = 35.023891;
const MIN_LATITUDE = 45;
const MAX_LATITUDE = 52;
const MIN_LONGITUDE = 15;
const MAX_LONGITUDE = 45;
const DESCRIPTIONS = [
        'Lorem ipsum dolor sit.',
        'Consectetur adipisicing elit, voluptatum.',
        'Accusantium adipisci animi aperiam iatur quibusdam ratione.',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, facilis, totam.',
        'ipsum dolor sit amet, consectetur adipisicing elit. Beatae consequuntur earum eligendi, rerum voluptate!',
        'Beatae consequuntur earum eligendi, excepturi hic maxime perspiciatis placeat, provident qui rerum voluptate',
        'consectetur adipisicing elit.',
        'Beatae consequuntu run'
    ];

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
}

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

let events = [];
for(let i=1; i < USERS_COUNT; i++){
    events = events.concat([...new Array(EVENTS_COUNT).keys()].map(()=>{
        return {
            user_id: i,
            description: DESCRIPTIONS[randomInt(0, DESCRIPTIONS.length-1)],
            latitude: getRandomFloat(MIN_LATITUDE, MAX_LATITUDE),
            longitude: getRandomFloat(MIN_LONGITUDE, MAX_LONGITUDE),
            createdAt: randomDate(new Date(2014, 0, 1), new Date()),
            updatedAt: new Date()
        };
    }));
}

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.bulkInsert('events', events, {});
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.bulkDelete('events', null, {});
  }
};
