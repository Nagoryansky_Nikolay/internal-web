'use strict';
const USERS_COUNT = 29;
const MIN_USER_ID = 1;
const MAX_USER_ID = 28;
const FRIENDS_COUNT = 10;
const STATUS = {
    FRIEND: 0
};

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

function getSequence(len, exception, min, max){
    let seq = {};
    let num;
    while (Object.keys(seq).length !== len){
        num = randomInt(min, max);
        if (num === exception){
            continue;
        }
        seq[num] = num;
    }
    return Object.keys(seq).map((i)=>{return parseInt(i)});

}

function isFriends(userId, friendId, friendships) {
    let status = false;
    if (!friendships.length){
        return false
    }
    for (let i=0; i< friendships.length; i++){
        if(friendships[i].user_id === userId && friendships[i].friend_id === friendId ){
            status = true;
            break;
        }
    }
    return status;
}

let friendships = [];
let chats = [];
for(let i=1; i < USERS_COUNT; i++){
    getSequence(FRIENDS_COUNT, i, MIN_USER_ID, MAX_USER_ID)
        .filter((uid)=>{ return !isFriends(i, uid, friendships)})
        .forEach((uid) => {
            friendships.push({
                user_id: i,
                friend_id: uid,
                status: STATUS.FRIEND,
                createdAt: new Date(),
                updatedAt: new Date()
            });
            friendships.push({
                user_id: uid,
                friend_id: i,
                status: STATUS.FRIEND,
                createdAt: new Date(),
                updatedAt: new Date()
            });
            chats.push({
                member1: i,
                member2: uid,
                createdAt: new Date(2013, 0, 1),
                updatedAt: randomDate(new Date(2014, 0, 1), new Date())
            });

        });
}

module.exports = {
    up: function (queryInterface, Sequelize) {
        return Promise.all([
            queryInterface.bulkInsert('friendships', friendships, {}),
            queryInterface.bulkInsert('chats', chats, {}),
        ])
    },

    down: function (queryInterface, Sequelize) {
        return Promise.all([
            queryInterface.bulkDelete('friendships', null, {}),
            queryInterface.bulkDelete('messages', null, {}),
            queryInterface.bulkDelete('chats', null, {}),
        ])
    }
};