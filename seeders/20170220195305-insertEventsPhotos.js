'use strict';
const EVENTS_COUNT = 561;
const MIN_PHOTO_COUNT = 1;
const MAX_PHOTO_COUNT = 6;
const PHOTOS = [
    "http://www.skiheavenly.com/~/media/heavenly/images/732x260%20header%20images/events-heavenly-header.ashx",
    "http://www.cih.org/resources/images/event_header.jpg",
    "http://mobilecausezone-mobilecause.netdna-ssl.com/wp-content/uploads/2015/12/event-fundraising-idea_50-50-raffle.png",
    "https://resources.stuff.co.nz/content/dam/images/1/e/5/z/v/8/image.related.StuffLandscapeSixteenByNine.620x349.1ex8i5.png/1476931263562.jpg",
    "http://www.mddir.com/hub/wp-content/uploads/2016/11/event-planning-company.jpg",
    "http://www.memorylaneweddings.com/sites/default/files/uploads/rustic-austin-texas-wedding-memory-lane-event-center-caroline-plus-ben-photography-41.jpg",
    "http://antcol.ru/images/event_management.jpg",
    "http://alwaysbusymama.com/images/content/1000-luchshie-meropriyatiya-dekabrya.jpg",
    "http://vidfilm.ru/cache/widgetkit/gallery/44/photographer%20videographer%20videography4-b3f65be168.jpg",
    "http://debbiehodge.com/home/wp-content/uploads/2010/07/iStock_familybday.jpg",
    "https://assets.dnainfo.com/generated/photo/2016/04/flag-day-2015-1461958681.jpg/larger.jpg",
    "http://simplelifecoaching.net/wp-content/uploads/2015/08/celebration.jpg",
    "http://www.spring-gds.com/nl/Images/celebrating-15-years-of-spring-news_tcm65-79872.jpg",
    "http://www.universityevents.harvard.edu/sites/universityevents.harvard.edu/files/img8.jpg"
];


function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

let totalPhotos = [];
for(let i=1; i < EVENTS_COUNT; i++){
    let photoIds = {};
    totalPhotos = totalPhotos.concat([...new Array(randomInt(MIN_PHOTO_COUNT, MAX_PHOTO_COUNT)).keys()]
        .map(()=>{
            const photoIndex = randomInt(0, PHOTOS.length-1);
            if (!photoIds[photoIndex]){
                photoIds[photoIndex] = 1;
                return {
                    event_id: i,
                    photo:  PHOTOS[photoIndex],
                    createdAt: new Date(),
                    updatedAt: new Date()
                };
            }

    }))
        .filter((photo)=>{
            return !!photo
        });

    if(!totalPhotos.length){
        totalPhotos.push({
            event_id: i,
            photo:  PHOTOS[0],
            createdAt: new Date(),
            updatedAt: new Date()
        });
    }
}

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.bulkInsert('eventPhotos', totalPhotos, {});
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.bulkDelete('eventPhotos', null, {});
  }
};
