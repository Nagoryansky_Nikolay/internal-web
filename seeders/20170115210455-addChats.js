'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.sequelize.query(`
            CREATE TABLE IF NOT EXISTS "chats" (
            "id"   SERIAL , 
            "member1" INTEGER, 
            "member2" INTEGER, 
            "countMessages" INTEGER, 
            "createdAt" TIMESTAMP WITH TIME ZONE NOT NULL, 
            "updatedAt" TIMESTAMP WITH TIME ZONE NOT NULL, 
            PRIMARY KEY ("id"));
        `
        );
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('chats');
    }
}