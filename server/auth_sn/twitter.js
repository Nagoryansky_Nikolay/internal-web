'use strict';

const passport = require('passport');
const TwitterStrategy = require('passport-twitter').Strategy;
const config = require('../../config');

function init() {
    passport.use(new TwitterStrategy({
            consumerKey        : config.sn.twitterAuth.consumerKey,
            consumerSecret    : config.sn.twitterAuth.consumerSecret,
            callbackURL     : config.sn.twitterAuth.callbackURL,
        },

        function(token, tokenSecret, profile, next) {
            process.nextTick(function () {
                let user = {};
                user.data = {
                    twitterId: profile.id,
                    email: profile._json.email  || '',
                    firstname: profile._json.first_name || '',
                    lastname: profile._json.last_name || '',
                    gender: profile._json.gender || '',
                    avatar: profile.photos[0].value || '',
                };
                user.id = profile.id;
                user.findCriteria = {
                    where: {
                        $or: [  {email: user.data.email},
                            {twitterId: profile.id}]
                    }
                };
                return next(null, user);
            });
        }));
}

module.exports = {
    init       : init
};