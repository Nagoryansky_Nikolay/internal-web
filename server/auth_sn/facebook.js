'use strict';

const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const config = require('../../config');
const guid = require('guid');



function init() {
    passport.use(new FacebookStrategy({
            // pull in our app id and secret from our auth.js file
            clientID        : config.sn.facebookAuth.clientID,
            clientSecret    : config.sn.facebookAuth.clientSecret,
            callbackURL     : config.sn.facebookAuth.callbackURL,
            profileFields: ['id', 'displayName', 'email', 'birthday', 'first_name', 'last_name', 'gender', 'picture']
        },

        function(token, tokenSecret, profile, next) {
            process.nextTick(function () {
                let user = {};
                user.data = {
                    facebookId: profile.id,
                    email: profile._json.email || '',
                    firstname: profile._json.first_name || '',
                    lastname: profile._json.last_name || '',
                    gender: profile._json.gender || '',
                    avatar: profile._json.picture.data.url ||  '',
                    birthday: profile._json.birthday ? convertDate(profile._json.birthday) : ''
                };
                user.id = profile.id;
                user.findCriteria = {
                    where: {
                        $or: [  {email: user.data.email},
                                {facebookId: profile.id}]
                    }
                };
                return next(null, user);
            });
        })
)
}

function convertDate(date){
    if (!date){
        return null
    }
    t = date.split('/');
    return t[0] + '-' + t[1] + '-' + t[0]

}
module.exports = {
    init       : init
};