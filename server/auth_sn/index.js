'use strict';
const facebook = require('./facebook');
const twitter = require('./twitter');
const google = require('./google');
const instagram = require('./instagram');
const Models = require('../models/v1');
const jwt = require('jsonwebtoken');
const guid = require('guid');
const Password = require('../utils/password');
const config = require('../../config/index');
const defaultFields = config.defaultUserFields;

function redirectToAuth(req, res) {
    let tokenParams = {};
    tokenParams.createTime = Date.now();
    tokenParams.id = req.user.id;
    let token = jwt.sign(tokenParams, config.jwt.user.jwtKey);
    let param = 'token=' + token;
    res.redirect('/auth?' + encodeURI(param));
}

function findOrCreate (req, res, next) {
    let socialUserData = fillUpUser(req.user.data);
    let findCriteria = req.user.findCriteria;

    Models.users.findOne(findCriteria)
        .then((user) => {
            if (user) {
                req.user = user;
                return next(); // user found, return that user
            }
            // save our user to the database
            return Models.users.create(socialUserData);
        })
        .then((newUser) => {
            // if successful, return the new user
            req.user = newUser;
            return next();
        })
        .catch(next)
}

function fillUpUser(rawUser){
    var user = {
        facebookId: rawUser.facebookId || '',
        googleId: rawUser.googleId || '',
        instagramId: rawUser.instagramId || '',
        twitterId: rawUser.twitterId || '',
        username: rawUser.username ||  guid.raw(),
        email: rawUser.email || guid.raw() +  defaultFields.EMAIL_DOMAIN,
        password: rawUser.password || Password.hash(defaultFields.PASSWORD) ,
        firstname: rawUser.firstname || defaultFields.FIRST_NAME,
        lastname: rawUser.lastname || defaultFields.LAST_NAME,
        gender: rawUser.gender !== defaultFields.FEMALE_GENDER,
        avatar: rawUser.avatar ||  '',
        birthday: rawUser.birthday || null
    };
    return user;
}

module.exports = {
    facebook : facebook,
    twitter: twitter,
    google: google,
    instagram: instagram,
    findOrCreate: findOrCreate,
    redirectToAuth: redirectToAuth,

};