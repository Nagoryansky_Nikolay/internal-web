'use strict';

const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const config = require('../../config');

function init() {
    passport.use(new GoogleStrategy({
            clientID        : config.sn.googleAuth.clientID,
            clientSecret    : config.sn.googleAuth.clientSecret,
            callbackURL     : config.sn.googleAuth.callbackURL,
        },

        function(token, tokenSecret, profile, next) {
            process.nextTick(function () {
                let user = {};
                user.data = {
                    googleId: profile.id,
                    email: profile.emails[0].value || '',
                    firstname: profile.name.familyName || '',
                    lastname: profile.name.givenName || '',
                    gender: profile._json.gender || '',
                    avatar: profile.photos[0].value || '',
                };
                user.id = profile.id;
                user.findCriteria = {
                    where: {
                        $or: [  {email: user.data.email},
                                {googleId: profile.id}]
                    }
                };
                return next(null, user);
            });
        }));
}

module.exports = {
    init       : init
};