'use strict';

const passport = require('passport');
const InstagramStrategy = require('passport-instagram').Strategy;
const config = require('../../config');

function init() {
    passport.use(new InstagramStrategy({
            // pull in our app id and secret from our auth.js file
            clientID        : config.sn.instagramAuth.clientID,
            clientSecret    : config.sn.instagramAuth.clientSecret,
            callbackURL     : config.sn.instagramAuth.callbackURL,
        },

        function(token, tokenSecret, profile, next) {
            process.nextTick(function () {
                let user = {};
                user.data = {
                    instagramId: profile.id,
                    firstname: profile.name.familyName || '',
                    lastname: profile.name.givenName || '',
                    gender: profile._json.gender || '',
                    avatar: profile._json.data.profile_picture || '',
                };
                user.id = profile.id;
                user.findCriteria = {
                    where: {
                        $or: [  {email: user.data.email},
                                {instagramId: profile.id}]
                    }
                };
                return next(null, user);
            });
        }));
}

module.exports = {
    init       : init
};