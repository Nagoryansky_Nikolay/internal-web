'use strict';

const Controller = require('../../utils/controller');
const Models = require('../../models/v1');
const config = require('../../../config');
const awsS3Service = require('../../services/awsS3Service');
const guid = require('guid');
const EVENT_LIMIT = 20;
const FEED_LIMIT = 200;
const RADIUS = 200000; //meters

class EventsController extends Controller {
    constructor(version) {
        super(version);

        this.addEvent = [
            this.validators.events.add,
            this._getUploadFilesParams,
            this._uploadFiles,
            this._createEvent,
            this._createEventPhotos
        ];
        this.removeEvent = [
            this._checkAccess,
            this._removeEvent
        ];
        this.getAllEvents = [
            this._getUser,
            this._getAllEvents
        ];
        this.getFeed = [
            this._parseQueryParams,
            this._getFeed
        ];

    }

    _getUser(req, res, next) {
        if (req.query.uid === 'me' || req.query.uid == res.locals.authUser.id){
            res.locals.user = res.locals.authUser;
            return next();
        }
        Models.users.findById(req.query.uid)
            .then(function (user) {
                if (!user) {
                    return res.status(404).send({message: 'User not found'});
                }
                res.locals.user = user;
                next();
            })
            .catch(next);
    }

    _checkAccess(req, res, next){
        Models.events.findById(req.body.id)
            .then(function (event) {
                if (!event) {
                    return res.status(404).send({message: 'Event does not found'});
                }
                if (event.userId !== res.locals.authUser.id) {
                    return res.status(404).send({message: 'Access denied, You can\'t remove this event!'});
                }
                res.locals.event = event;
                next();
            })
            .catch(next);
    }

    _removeEvent(req, res, next){
        res.locals.event.update({isDeleted: true})
            .then((event)=>{
                return res.status(200).send({
                    message: 'You have successfully deleted the event!',
                    id: event.id
                });
            })
            .catch(next);
    }

     _getAllEvents(req, res, next){
         const limit = req.query.limit ? req.query.limit : EVENT_LIMIT;
         const offset = req.query.offset ? req.query.offset : 0;
         res.locals.user.getEvents({
             where:{
                 isDeleted: false
             },
             limit : limit,
             offset: offset,
             order: [['createdAt', 'DESC']],
             include: [
                 Models.eventPhotos
             ]
         })
             .then((eventList)=>{
                let events = Models.events.format.base(eventList).map((event)=>{
                    event.user = Models.users.format.short(res.locals.user);
                    return event
                });
                 return res.status(201).send({
                     events: events
                 });

             })
             .catch(next);
    }

    _getUploadFilesParams(req, res, next){
        if (!req.body.photos || !req.body.photos.length){
            return next();
        }
        res.locals.uploadFileParams = req.body.photos.map((photo)=>{
            return {
                buf: new Buffer(photo.file.replace(/^data:image\/\w+;base64,/, ""),'base64'),
                contentType: photo.file.match(/^data:(image\/\w+)/i)[1],
                contentEncoding: 'base64',
                filename: guid.raw() + (photo.filename || ''),
                acl: 'public-read'
            }
        });
        next();
    }

    _uploadFiles(req, res, next){
        if (!req.body.photos || !req.body.photos.length){
            return next();
        }
        res.locals.eventPhotos = [];
        res.locals.count = res.locals.uploadFileParams.length;
        res.locals.uploadFileParams.forEach((param)=>{
            let key = config.s3.EVENTS_PATH + param.filename;
            awsS3Service.uploadFile(key, param.buf, param.contentType, param.contentEncoding, param.acl)
                .then((response) =>{
                    console.log(param.filename);
                    res.locals.eventPhotos.push(param.filename);
                    res.locals.count --;
                    if (res.locals.count === 0){
                        next()
                    }
                })
                .catch((err) => {
                    return res.status(406).send({
                        message: err,
                    })
                });
        });
    }

    _createEvent(req, res, next){
        let params = {
            userId: res.locals.authUser.id,
            description: req.body.description,
            latitude: req.body.lat || null,
            longitude: req.body.lng || null,
        };

        Models.events.create(params)
            .then(event => {
                res.locals.event = event;
                next();
            })
            .catch(next);
    }

    _createEventPhotos(req, res, next){
        if (!req.body.photos || !req.body.photos.length){
            let event = Models.events.format.base(res.locals.event);
            event.user = Models.users.format.short(res.locals.authUser);
            return res.status(201).send({
                message: "You are now successfully added event",
                event: event
            });
        }
        const photos = res.locals.eventPhotos.map((photo)=>{
            return {
                eventId: res.locals.event.id,
                photo: photo
            }
        });
        Models.eventPhotos.bulkCreate(photos)
            .then(photos => {
                let event = Models.events.format.base(res.locals.event);
                event.user = Models.users.format.short(res.locals.authUser);
                event.photos = Models.eventPhotos.format.base(photos).map((item)=> item.photo);
                return res.status(201).send({
                    message: "You are now successfully added event",
                    event: event
                });
            })
            .catch(next);
    }

    _parseQueryParams(req, res, next){
        res.locals.where = {
            isDeleted: false
        };
        if (req.query.lat && req.query.lng){
            res.locals.where = Models.sequelize.and(
                Models.sequelize.where(
                    Models.sequelize.fn('earth_box', Models.sequelize.fn('ll_to_earth', req.query.lat, req.query.lng), RADIUS),
                    '@>',
                    Models.sequelize.fn('ll_to_earth', Models.sequelize.col('"events"."latitude"'), Models.sequelize.col('"events"."longitude"'))
                ),
                {
                    isDeleted: false
                })
        }
        next();
    }

    _getFeed(req, res, next){
        const limit = req.query.limit ? req.query.limit : FEED_LIMIT;
        const offset = req.query.offset ? req.query.offset : 0;
        Models.events.findAll({
            where: res.locals.where,
            order: [
                ['createdAt', 'DESC']
            ],
            limit,
            offset,
            include: [
                Models.eventPhotos,
                {
                    model: Models.users,
                    required: true,
                    include: [
                        {
                            model: Models.users,
                            required: true,
                            as: 'friends',
                            through: {
                                where:{
                                    status: {
                                        $in: [0, 2]
                                    }
                                }
                            },
                            where: {
                                id: res.locals.authUser.id
                            }
                        }
                    ]
                }
            ]
        })
            .then((eventList)=>{
                let events = Models.events.format.base(eventList).map((event)=>{
                    return event
                });
                return res.status(200).send({
                    events: events
                });

            })
            .catch(next);
    }


}
module.exports = EventsController;
