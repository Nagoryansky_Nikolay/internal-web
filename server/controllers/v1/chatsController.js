'use strict';

const Controller = require('../../utils/controller');
const Models = require('../../models/v1');
const config = require('../../../config');
const chatModels = require('../../models_chat/v1');

const CHAT_LIMIT = 200;
const MESSAGE_LIMIT = 50;

class ChatsController extends Controller {
    constructor(version) {
        super(version);

        this.getAllChats = [
            this._parseQueryParams,
            this._getAllChats,
            this._getChatUsers,
            this._sendChats
        ];

        this.getAllMessages = [
            this._getChatById,
            this._getUserById,
            this._getAllMessages
        ];

    }

    _formatChats(chats, currentUid){
        if (!chats){
            return []
        }
        function structure(chat){
            const uid = (chat.member1 === currentUid) ? chat.member2 : chat.member1;
            return {
                id: chat.id || chat._id,
                updatedAt: chat.updatedAt,
                createdAt: chat.createdAt,
                user: {
                    id : uid
                }
            }
        }

        if (Array.isArray(chats)) {
            return chats.map(chat => structure(chat));
        }
        return structure(chats);
    }

    _formatMessage(messages){
        if (!messages){
            return []
        }
        function structure(message){
            return {
                id: message.id || message._id,
                text: message.text,
                senderId: message.senderId,
                chatId: message.chatId,
                createdAt: message.createdAt,
            }
        }

        if (Array.isArray(messages)) {
            return messages.map(message => structure(message));
        }
        return structure(messages);
    }

    _parseQueryParams(req, res, next){
        let params = {};

        if (req.query.uid) {
            params.$or = [
                {member1: res.locals.authUser.id, member2: req.query.uid},
                {member2: res.locals.authUser.id, member1: req.query.uid}
            ]
        }
        else{
            params.$or = [{member1: res.locals.authUser.id}, {member2:res.locals.authUser.id}];
            params.countMessages = {$gt: 0};
        }

        res.locals.params = params;
        next()
    };


    _getAllChats(req, res, next){
        const limit = req.query.limit ? req.query.limit : CHAT_LIMIT;
        const offset = req.query.offset ? req.query.offset : 0;
        chatModels.chats.findAll(
                res.locals.params,
                limit,
                offset,
                {updatedAt: 'desc'}
            )
            .then(data => {
                res.locals.chats = this._formatChats(data, res.locals.authUser.id);
                return next()
            })
            .catch(next);
    }

    _getChatUsers(req, res, next){
        const ids = res.locals.chats.map(chat => chat.id);
        Models.users.findAll({id : {$in : ids}})
            .then(users => {
                res.locals.chatUsers = Models.users.format.base(users);
                return next()
            })
            .catch(next);
    }

    _sendChats(req, res, next){
        const chats = res.locals.chats.map(chat => {
            const user = res.locals.chatUsers.filter(user => chat.user.id == user.id)[0];
            chat.user.avatar = user.avatar;
            chat.user.firstname = user.firstname;
            chat.user.lastname = user.lastname;
            return chat;
        });

        return res.status(200).send({
            totalCount: res.locals.totalCount,
            chats:  chats
        })
    }

    _getChatById(req, res, next){
        chatModels.chats.findById(req.params.id)
            .then(chat => {
                if (!chat) {
                    return res.status(404).send({message: 'Chat not found'});
                }
                res.locals.chat =  this._formatChats(chat, res.locals.authUser.id);
                return next();
            })
            .catch(next);
    }

    _getUserById(req, res, next){
        Models.users.findById(res.locals.chat.user.id)
            .then(function (user) {
                if (!user) {
                    return res.status(404).send({message: 'User not found'});
                }
                res.locals.targetUser =  Models.users.format.base(user);
                next();
            })
            .catch(next);
    }

    _getAllMessages(req, res, next){
        const limit = req.query.limit ? req.query.limit : MESSAGE_LIMIT;
        const offset = req.query.offset ? req.query.offset : 0;

        Promise.all([
            chatModels.messages.findAll({chatId: req.params.id}, limit, offset, {createdAt: 'desc'}),
            chatModels.messages.count({chatId: req.params.id}),
        ])
            .then(data => {
                return res.status(200).send({
                    totalCount: parseInt(data[1]),
                    messages:   this._formatMessage(data[0]),
                    user: {
                        id: res.locals.targetUser.id,
                        lastname: res.locals.targetUser.lastname,
                        firstname: res.locals.targetUser.firstname,
                        avatar: res.locals.targetUser.avatar
                    }
                })
            })
            .catch(next);
    }


}
module.exports = ChatsController;
