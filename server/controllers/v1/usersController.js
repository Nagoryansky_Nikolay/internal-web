'use strict';

const Controller = require('../../utils/controller');
const Models = require('../../models/v1');
const Password = require('../../utils/password');
const config = require('../../../config');
const sendMail = require('../../services/sendMail');
const authService = require('../../services/authService');
const awsS3Service = require('../../services/awsS3Service');
const defaultFields = config.defaultUserFields;
const guid = require('guid');
const moment = require('moment');
const chatModels = require('../../models_chat/v1');
const jimp = require("jimp");


class UsersController extends Controller {
    constructor(version) {
        super(version);

        this.addUser = [
            this.validators.users.add,
            this._check,
            this._requirements,
            this._addUser,
            this._sendConfirmToken
        ];

        this.login = [
            this.validators.users.login,
            this._checkLoginPassword,
            this._authorization
        ];

        this.getById = [
            this._getById,
            this._setFriendStasus,
            this._sendUser

        ];

        this.confirmMail = [
            this._tryDecodeConfirmToken,
            this._checkConfirmToken,
            this._updateConfirmField,
            this._authorization
        ];

        this.checkUserToken = [
            this._tryDecodeUserToken,
            this._checkUserToken
        ];

        this.restoreRequest = [
            this.validators.users.restoreRequest,
            this._checkUserEmail,
            this._sendRestoreLink
        ];

        this.restorePassword = [
            this.validators.users.restorePassword,
            this._tryDecodeRestoreToken,
            this._checkRestoreToken,
            this._changePassword
        ];

        this.updateUser = [
            this.validators.users.update,
            this._checkPassword,
            this._compareEmail,
            this._compareUsername,
            this._updateUser
        ];

        this.removeAvatar = [
            this._checkUserAccess,
            this._removeAvatar,
            this._updateAvatarField
        ];
        this.removeCover = [
            this._checkUserAccess,
            this._removeCoverImg,
            this._updateCoverField
        ];

        this.uploadUserAvatar = [
            this._checkUserAccess,
            this._getUploadPath,
            this._getUploadFileParams,
            this._getCroppedAvatar,
            this._uploadCroppedAvatar,
            this._uploadUserFile,
            this._updateAvatarField
        ];
        this.uploadUserCover = [
            this._checkUserAccess,
            this._getUploadPath,
            this._getUploadFileParams,
            this._uploadUserFile,
            this._updateCoverField
        ];
        this.getUsers = [
            this.validators.users.queryParams,
            this._parseQueryParams,
            this._makeNameFindCriteria,
            this._makeFindCriteria,
            this._getUsers,
            this._setFriendStatusToUsers,
            this._sendUsers
        ];
        this.getFriends = [
            this._getById,
            this._parseQueryParams,
            this._makeNameFindCriteria,
            this._getFriends,
            this._setFriendStatusToFriends,
            this._sendUsers

        ];
        this.addFriend = [
            this._isCurrentUserAuth,
            this._getTargetUserById,
            this._getFriendship,
            this._addFriend
        ];
        this.acceptFriend = [
            this._isCurrentUserAuth,
            this._getTargetUserById,
            this._getFriendship,
            this._acceptFriend,
            this._createChat
        ];
        this.removeFriend = [
            this._isCurrentUserAuth,
            this._getTargetUserById,
            this._getFriendship,
            this._removeFriend,
        ];
    }

    _isCurrentUserAuth(req, res, next){
        if (req.params.id === 'me' || req.params.id == res.locals.authUser.id){
            res.locals.currentUser = res.locals.authUser;
            return next();
        }
        return res.status(403).send({message: 'Error! Access denied for user '});
    }

    _getById(req, res, next) {
        if (req.params.id === 'me' || req.params.id === res.locals.authUser.id){
            res.locals.currentUser = res.locals.authUser;
            return next();
        }
        Models.users.findById(req.params.id)
            .then(function (user) {
                if (!user) {
                    return res.status(404).send({message: 'User not found'});
                }
                res.locals.currentUser = user;
                next();
            })
            .catch(next);
    }

    _setFriendStasus(req, res, next) {
        res.locals.currentUser.getFriends({where:{id:res.locals.authUser.id}})
            .then(function (user) {
                let status =  Models.friendships.STATUSES().NULL;
                if (user[0]){
                    status = user[0].friendships.status;
                }
                res.locals.currentUser = Models.users.format.base(res.locals.currentUser);
                res.locals.currentUser.friendshipStatus = status;
                next();

            })
            .catch(next);
    }


    _getTargetUserById(req, res, next) {
        if (req.body.id == res.locals.authUser.id){
            return res.status(403).send({message: 'Error! You can\'t add to friends yourself'});
        }
        Models.users.findById(req.body.id)
            .then(function (user) {
                if (!user) {
                    return res.status(404).send({message: 'User not found'});
                }
                res.locals.targetUser = user;
                next();
            })
            .catch(next);
    }

    _sendUser(req,res, next){
        return res.send(res.locals.currentUser);
    }

    _parseQueryParams(req, res, next){
        let params = {};
        params.limit = req.query.limit ? req.query.limit : config.PAGINATION_LIMIT;
        params.offset = req.query.offset ? req.query.offset : 0;

        if (req.query.name) {
            params.name = req.query.name.split(/\s+/);
        }
        if (req.query.gender === defaultFields.FEMALE_GENDER){
            params.gender = false;
        }
        if (req.query.gender === defaultFields.MALE_GENDER){
            params.gender = true;
        }
        if (!req.query.min_age && !req.query.max_age){
            res.locals.params = params;
            return next()
        }
        params.birthdayMin = req.query.min_age ?
            moment().subtract(req.query.min_age, 'years').format('YYYY-MM-DD') :
            moment().subtract(defaultFields.MIN_AGE, 'years').format('YYYY-MM-DD');
        params.birthdayMax = req.query.max_age ?
            moment().subtract(req.query.max_age, 'years').format('YYYY-MM-DD'):
            moment().subtract(defaultFields.MAX_AGE, 'years').format('YYYY-MM-DD');
        res.locals.params = params;
        next()
    };

    _makeNameFindCriteria(req, res, next){
        let params = res.locals.params;
        let nameFindCriteria = [];
        if (params.name && params.name.length > 1) {
            nameFindCriteria = [
                {
                    firstname: { $iLike: `%${params.name[0]}%` },
                    lastname: { $iLike: `%${params.name[1]}%` }
                },
                {
                    firstname: { $iLike: `%${params.name[1]}%` },
                    lastname: { $iLike: `%${params.name[0]}%` }
                }
            ];
        }else if(params.name){
            nameFindCriteria = [
                {
                    firstname: { $iLike: `%${params.name}%` }
                },
                {
                    lastname: { $iLike: `%${params.name}%` }
                }
            ]
        }
        res.locals.nameFindCriteria = nameFindCriteria;
        next();
    }

    _makeFindCriteria(req, res, next){
        let params = res.locals.params;
        let where = {};
        if (params.name) where.$or = res.locals.nameFindCriteria;

        if ( typeof(params.gender) === 'boolean' ){
            where.gender = params.gender;
        }
        if (params.birthdayMax){
            where.birthday = {
                $between: [params.birthdayMax, params.birthdayMin]
            };
        }
        where.id = {$ne: res.locals.authUser.id};

        res.locals.where = where;
        next();

    }

    _getUsers(req, res, next) {
        let limit = res.locals.params.limit;
        let offset = res.locals.params.offset;
        Promise.all([
            Models.users.scope({method: ['getUsers', Models, limit, offset, res.locals.where]}).findAll(),
            Models.users.count({
                where: res.locals.where
            })
        ])
            .then(data => {
                res.locals.users = Models.users.format.base(data[0]);
                res.locals.usersTotalCount = data[1];
                next()
            })
            .catch(next);
    }

    _getFriends(req, res, next) {
        let params = {};
        params.limit = res.locals.params.limit;
        params.offset = res.locals.params.offset;
        params.order = [['followers_count', 'DESC']];
        params.where = {};
        if (res.locals.params.name) params.where.$or = res.locals.nameFindCriteria;
        Promise.all([
            res.locals.authUser.getFollowers(),
            res.locals.currentUser.getFriends(params),
            res.locals.currentUser.countFriends({where:params.where}),
        ])
            .then(data => {
                res.locals.authUserFollowers = Models.users.format.base(data[0]);
                res.locals.currentUserFriends = Models.users.format.base(data[1]);
                res.locals.usersTotalCount = data[2];
                next()
            })
            .catch(next);
    }
    _setFriendStatusToUsers(req, res, next){
        let tmpUsers = res.locals.users.map(function(user) {
            let friends = user.friends;
            delete user.friends;
            user.friendshipStatus = Models.friendships.STATUSES().NULL;
            for(let i=0; i < friends.length; i++ ){
                if (friends[i].id == res.locals.authUser.id){
                    user.friendshipStatus = friends[i].friendshipStatus;
                }
            }
            return user
        });
        res.locals.users = tmpUsers;
        next();
    }

    _setFriendStatusToFriends(req, res, next){
        let tmpUsers = res.locals.currentUserFriends.map(function(user) {
            for(let i=0; i < res.locals.authUserFollowers.length; i++ ){
                if (res.locals.authUserFollowers[i].id === user.id){
                    user.friendshipStatus = res.locals.authUserFollowers[i].friendshipStatus;
                    break;
                }else{
                    user.friendshipStatus = Models.friendships.STATUSES().NULL;
                }
            }
            return user
        });
        res.locals.users = tmpUsers;
        next();
    }

    _sendUsers(req, res, next){
        return res.status(200).send({
            users:  res.locals.users,
            totalCount: res.locals.usersTotalCount
        })
    }

    _getFriendship(req, res, next){
        Models.friendships.findOne({
            where: {
                userId: res.locals.currentUser.id,
                friendId: res.locals.targetUser.id,
            }
        })
            .then(friendship => {
                res.locals.friendship = friendship;
                next();
            })
            .catch(next);
    }

    _addFriend(req, res, next){
        if (res.locals.friendship){
            return res.status(400).send({
                message: 'Error! You have already added user to friends'
            })
        }
        Promise.all([
            res.locals.currentUser.addFriend(req.body.id, {status:Models.friendships.STATUSES().FOLLOWER}),
            res.locals.currentUser.addFollower(req.body.id, {status:Models.friendships.STATUSES().FOLLOWEE}),
            res.locals.targetUser.update({followersCount: res.locals.targetUser.followersCount + 1})
        ])
            .then(data => {
                let user = Models.users.format.base(res.locals.targetUser);
                user.friendshipStatus = Models.friendships.STATUSES().FOLLOWEE;
                return res.status(201).send({
                    user: user
                })
            })
            .catch(next)
    }


    _acceptFriend(req, res, next){
        if (!res.locals.friendship || res.locals.friendship.status !== Models.friendships.STATUSES().FOLLOWEE){
            return res.status(406).send({
                message: 'Error! You can not make this operation',
            });
        }
        Promise.all([
            res.locals.targetUser.update({followersCount: res.locals.targetUser.followersCount + 1}),
            Models.friendships.update(
                {status: 0},
                {where: {
                    $or: [
                        {
                            userId: res.locals.currentUser.id,
                            friendId: res.locals.targetUser.id
                        },
                        {
                            friendId: res.locals.currentUser.id,
                            userId: res.locals.targetUser.id
                        }
                    ]
                }})
        ])
        .then(data => {
            let user = Models.users.format.base(res.locals.targetUser);
            user.friendshipStatus = Models.friendships.STATUSES().FRIEND;
            res.locals.user = user;
            next()

        })
        .catch(next);
    }

    _createChat(req, res, next){
        chatModels.chats.findOne({
            $or:[
                {member1: res.locals.currentUser.id, member2: res.locals.targetUser.id},
                {member2: res.locals.currentUser.id, member1:res.locals.targetUser.id}
            ]}
        )
            .then(chat => {
                if (chat ){
                    return res.status(201).send({user: res.locals.user})
                }
                chatModels.chats.create({member1: res.locals.currentUser.id, member2: res.locals.targetUser.id, countMessages:0})
                    .then(() => {
                        return res.status(201).send({user: res.locals.user})
                    })
            })
            .catch(next);
    }

    _removeFriend(req, res, next){
        if (!res.locals.friendship || res.locals.friendship.status === Models.friendships.STATUSES().FOLLOWEE){
            return res.status(406).send({
                message: 'Error! You can not remove this user',
            });
        }
        if (res.locals.friendship.status === Models.friendships.STATUSES().FRIEND){
            Promise.all([
                Models.friendships.update(
                    {status: 2},
                    {where: {
                        userId: res.locals.currentUser.id,
                        friendId: res.locals.targetUser.id,
                    }}),
                Models.friendships.update(
                    {status: 1},
                    {where: {
                        friendId: res.locals.currentUser.id,
                        userId: res.locals.targetUser.id,
                    }}),
                res.locals.targetUser.update({followersCount: res.locals.targetUser.followersCount - 1})
            ])
                .then(data => {
                    let user = Models.users.format.base(res.locals.targetUser);
                    user.friendshipStatus = Models.friendships.STATUSES().FOLLOWER;
                    return res.status(201).send({
                        user: user
                    })
                })
                .catch(next);
        }
        if (res.locals.friendship.status === Models.friendships.STATUSES().FOLLOWER){
            Promise.all([
                Models.friendships.destroy(
                    {where: {
                        $or: [
                            {
                                userId: res.locals.currentUser.id,
                                friendId: res.locals.targetUser.id
                            },
                            {
                                friendId: res.locals.currentUser.id,
                                userId: res.locals.targetUser.id
                            }
                        ]
                    }}),
                res.locals.targetUser.update({followersCount: res.locals.targetUser.followersCount - 1})
            ])
                .then(data => {
                    let user = Models.users.format.base(res.locals.targetUser);
                    user.friendshipStatus = Models.friendships.STATUSES().NULL;
                    return res.status(201).send({
                        user: user
                    })
                })
                .catch(next);
        }
    }

    _check(req, res, next) {
        Models.users.findOne({
            where: {
                $or: [
                    {email: req.body.email},
                    {username: req.body.username}]
            },
            attributes: ['username', 'email']
        })
            .then(user => {
                res.locals.user = user;
                next();
            })
    }

    _requirements(req, res, next) {
        let user = res.locals.user;
        if (user) {
            let message;
            if (user.username === req.body.username) {
                message = 'An account with this username already exists';
            } else {
                message = 'An account with this email address already exists';
            }
            return res.status(409).send({
                message: message,
            });
        }
        next();
    }

    _addUser(req, res, next) {
        let params = {
            email: req.body.email,
            username: req.body.username,
            password: Password.hash(req.body.password),
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            avatar: '',
            isConfirmEmail: false
        };

        Models.users.create(params)
            .then(user => {
                res.locals.user = user;
                next();
            })
            .catch(next);
    }

    _sendConfirmToken(req, res, next) {
        let user = res.locals.user;
        let token = this.createToken(user.id, config.jwt.confirmMail.jwtKey);
        let url = config.server.baseUrl + '/confirm-mail?token=' + token;
        let mailOptions = {
            from: `Internal-web project <${config.mail.auth.user}>`,
            to: user.email,
            subject: 'Welcome to Internal-web project',
            text: '',
            html: sendMail.getConfirmMessage(user.firstname, url)
        };
        sendMail.send(mailOptions)
            .then(() => {
                res.status(201).send({
                    message: "Great! You are now successfully registered. Please confirm your email address",
                    user: Models.users.format.base(user)
                });
            })
            .catch(() => {
                req.locals.user.destroy();
                res.status(406).send({
                    message: 'Error registration, try again(',
                });
            });
    }

    _checkLoginPassword(req, res, next) {
        let email = req.body.email;
        let password = req.body.password;

        Models.users.findOne({
            where: {
                email: email
            }
        })
            .then(user => {
                if (!user) {
                    return res.status(401).send({
                        message: 'Invalid email or password'
                    });
                }
                try {
                    var passwordIsValid = Password.compare(password, user.password);
                }
                catch (err) {
                    return res.status(422).send({
                        message: 'Error!(',
                    });
                }
                if (!passwordIsValid) {
                    return res.status(401).send({
                        message: 'Invalid email or password',
                    });
                }
                res.locals.user = user;
                next();
            });
    }


    _authorization(req, res, next) {
        let token = this.createToken(res.locals.user.id, config.jwt.user.jwtKey);
        let response = {
            user: Models.users.format.base(res.locals.user),
            session: {
                accessToken: token
            }
        };
        return res.send(response);
    }

    _checkPassword(req, res, next){
        if (!req.body.password){
            res.locals.new_password = res.locals.authUser.password;
            return next();
        }
        if (req.body.password && !req.body.new_password) {
            return res.status(400).send({
                message: 'Field new_password is required',
            });
        }
        if (!req.body.password && req.body.new_password){
            return res.status(400).send({
                message: 'Field password is required',
            });
        }
        try {
            var passwordIsValid = Password.compare(req.body.password, res.locals.authUser.password);
        }
        catch (err) {
            return res.status(422).send({
                message: 'Error!(',
            });
        }
        if (!passwordIsValid) {
            return res.status(406).send({
                message: 'Invalid password',
            });
        }
        res.locals.new_password = Password.hash(req.body.new_password);
        next();

    }
    _compareEmail(req, res, next){
        if  (req.body.email === res.locals.authUser.email){
            return next();
        }
        Models.users.findOne({
            where: {email: req.body.email}
        })
            .then(user => {
                if (user) {
                    return res.status(409).send({
                        message: 'An account with this email address already exists',
                    });
                }
                next();
            })
            .catch(next);
    }

    _compareUsername(req, res, next){
        if (req.body.username !== res.locals.authUser.username){
            Models.users.findOne({
                where: {username: req.body.username}
            })
                .then(user => {
                    if (user) {
                        return res.status(409).send({
                            message: 'An account with this username already exists',
                        });
                    }
                })
                .catch(next);
        }
        next();
    }

    _updateUser(req, res, next) {
        let gender = null;
        if (req.body.gender === 'Female') {gender =false;}
        if (req.body.gender === 'Male') {gender =true;}
        let params = {
            email: req.body.email,
            username: req.body.username,
            password: res.locals.new_password,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            gender: gender,
            birthday: req.body.birthday || null,
            city: req.body.city || "",
            country: req.body.country || "",
        };
        res.locals.authUser.update(params)
            .then( user => {
                   return res.status(201).send(Models.users.format.base(res.locals.authUser));
                })
            .catch(next);
    }

    _tryDecodeConfirmToken(req, res, next) {
        let token = req.params.token ;
        let data = authService.decodeToken(token, config.jwt.confirmMail.jwtKey);
        if (!data) {
            return res.status(401)
                .send({message: 'Token is not valid'});
        }
        res.locals.tokenData = data;
        next()
    }

    _tryDecodeUserToken(req, res, next) {
        let token = req.get('x-session-token') ;
        let data = authService.decodeToken(token, config.jwt.user.jwtKey);
        if (!data) {
            return res.status(401)
                .send({message: 'User token is not valid'});
        }
        res.locals.tokenData = data;
        next()
    }

    _tryDecodeRestoreToken(req, res, next) {
        let token = req.params.token ;
        let data = authService.decodeToken(token, config.jwt.restorePassword.jwtKey);
        if (!data) {
            return res.status(401)
                .send({message: 'Link is broken, try again'});
        }
        res.locals.tokenData = data;
        next()
    }

    _checkConfirmToken(req, res, next) {
        Models.users.findById(res.locals.tokenData.id)
            .then(user => {
                if (!user) {
                    return res.status(410).send({message: 'Confirm token is expired, try registration again...'})
                }
                if (user.isConfirmEmail) {
                    return res.status(409).send({message: 'You have already confirmed your email!'})
                }
                res.locals.user = user;
                next();
            })
            .catch(next);
    }

    _updateConfirmField(req, res, next) {
        res.locals.user.update({
            isConfirmEmail: true,
        })
            .then(() => next())
            .catch(next);
    }

    _checkUserToken(req, res, next) {
        let timeDelta = Date.now() - res.locals.tokenData.createTime;
        if (timeDelta > config.jwt.user.jwtLifeTime) {
            return res.status(410).send({message: 'User token is expired!'})
        }
        Models.users.findById(res.locals.tokenData.id)
            .then(user => {
                if (user) {
                    return res.status(200).send({message: 'Ok',user: Models.users.format.base(user)})
                }
                return res.status(401).send({message: 'Unknown user token'})
            })
            .catch(next);
    }

    _checkUserEmail(req, res, next) {
        Models.users.findOne({
            where: {
                email: req.body.email
            }
        })
            .then(user => {
                if (!user) {
                    return res.status(404).send({message: 'The account with this email address does not exists!'})
                }
                res.locals.user = user;
                next();
            })
            .catch(next)
    }

    _sendRestoreLink(req, res, next) {
        let user = res.locals.user;
        let token = this.createToken(user.id, config.jwt.restorePassword.jwtKey);
        let url = config.server.baseUrl + '/restore-password?token=' + token;
        let mailOptions = {
            from: `Internal-web project <${config.mail.auth.user}>`,
            to: user.email,
            subject: 'Reset your password on Internal-web project',
            text: '',
            html: sendMail.getRestoreMessage(user.firstname, url)
        };
        sendMail.send(mailOptions)
            .then(() => {
                return res.status(201).send({
                    message: "We just sent you a confirmation email. Please check your email!",
                    user: Models.users.format.base(user)
                });
            })
            .catch(() => {
                return res.status(406).send({
                    message: 'Error, try again(',
                });
            });
    }

    _checkRestoreToken(req, res, next) {
        let timeDelta = Date.now() - res.locals.tokenData.createTime;
        if (timeDelta > config.jwt.restorePassword.jwtLifeTime) {
            return res.status(410).send({message: 'Token is expired!'})
        }

        Models.users.findOne({
            where: {
                email: req.body.email
            }
        })
            .then(user => {
                if (!user || (user.id !== res.locals.tokenData.id)) {
                    return res.status(404).send({message: 'Email address doesn\'t match access token!'})
                }
                res.locals.user = user;
                next();
            })
            .catch(next)
    }

    _changePassword(req, res, next) {
        let password = Password.hash(req.body.password);
        res.locals.user.update({
            password: password
        })
            .then((user) =>{
                return res.status(201).send({
                    message: "Password successfully changed",
                })
            })
            .catch(next);
    }

    _checkUserAccess(req, res, next){
        if ( req.params.id !== 'me' && req.params.id !== res.locals.authUser.id ){
            return res.status(401).send({message: 'Access token does not match user id'})
        }
        next();
    }

    _removeAvatar(req, res, next){
        let avatar = req.body.avatar;
        if (!avatar.startsWith(config.s3.bucketUrl) || avatar === defaultFields.AVATAR_URL){
            res.locals.avatar = null;
            return next();
        }

        let key = req.body.avatar.replace(config.s3.bucketUrl, '');
        awsS3Service.deleteFile(key)
            .then((response) =>{
                res.locals.avatar = defaultFields.AVATAR;
                return next();
            })
            .catch((response) => {
                return res.status(406).send({
                    message: response,
                })
            });
    }

    _removeCoverImg(req, res, next){
        let cover = req.body.cover;
        if (!cover.startsWith(config.s3.bucketUrl) || cover === defaultFields.COVER_URL){
            res.locals.cover = null;
            return next();
        }

        let key = cover.replace(config.s3.bucketUrl, '');
        awsS3Service.deleteFile(key)
            .then((response) =>{
                res.locals.cover = defaultFields.COVER;
                return next();
            })
            .catch((response) => {
                return res.status(406).send({
                    message: response,
                })
            });
    }

    _getUploadPath(req, res, next){
        let url = req.originalUrl.split('/');
        let endpoint = url[url.length-1].split('?')[0];
        let uploadPath = '';
        if (endpoint === 'avatar'){ uploadPath = config.s3.AVATARS_PATH}
        else if (endpoint === 'cover'){ uploadPath = config.s3.COVERS_PATH}
        res.locals.uploadPath = uploadPath;
        next();
    }

    _getUploadFileParams(req, res, next){
        const uploadFileParams = {
            buf: new Buffer(req.body.file.replace(/^data:image\/\w+;base64,/, ""),'base64'),
            contentType: req.body.file.match(/^data:(image\/\w+)/i)[1],
            contentEncoding: 'base64',
            filename: guid.raw() + (req.body.filename || ''),
            acl: 'public-read'
        };
        res.locals.uploadFileParams = uploadFileParams;
        next();
    }

    _getCroppedAvatar(req, res, next){
        jimp.read(res.locals.uploadFileParams.buf)
            .then((img) => {
                img.resize(100, 100)
                    .quality(80)
                    .getBuffer(res.locals.uploadFileParams.contentType, (e, buf) => {
                        res.locals.croppedAvatarBuf = buf;
                        next();
                    })
            })
            .catch(err => console.log(err));
    }
    
    _uploadCroppedAvatar(req, res, next){
        let { contentType, contentEncoding, filename, acl } = res.locals.uploadFileParams;
        let key = config.s3.SM_AVATARS_PATH + filename;
        awsS3Service.uploadFile(key, res.locals.croppedAvatarBuf, contentType, contentEncoding, acl)
            .then(() =>{
                return next();
            })
            .catch((err) => {
                return res.status(406).send({message: err})
            });
    }

    _uploadUserFile(req, res, next){
        let { buf, contentType, contentEncoding, filename, acl } = res.locals.uploadFileParams;
        let key = res.locals.uploadPath + filename;

        awsS3Service.uploadFile(key, buf, contentType, contentEncoding, acl)
            .then((response) =>{
                res.locals.uploadedImgName = filename;
                return next();
            })
            .catch((err) => {
                return res.status(406).send({
                    message: err,
                })
            });
    }

    _updateAvatarField(req, res, next){
        let avatar = res.locals.uploadedImgName || res.locals.avatar;
        res.locals.authUser.update({
            avatar: avatar
        })
            .then((user) =>{
                return res.status(201).send({
                    message: "Avatar was successfully changed",
                    user: Models.users.format.base(user)
                })
            })
            .catch(next);
    }

    _updateCoverField(req, res, next){
        res.locals.authUser.update({
            cover: res.locals.uploadedImgName || res.locals.cover
        })
            .then((user) =>{
                return res.status(201).send({
                    message: "Cover was successfully changed",
                    user: Models.users.format.base(user)
                })
            })
            .catch(next);
    }

}
module.exports = UsersController;
