'use strict';

var jwt = require('jsonwebtoken');
var config = require('../../config');

class Controller {
    constructor (version) {
        this.VERSION = version;

        /**
         * validate skip and count parameters for getting any resource
         * @param req
         * @param res
         * @param next
         * @returns {*}
         * @private
         */
        this.validateLimits = function (req, res, next) {
            req.query.limit = parseInt(req.query.limit) ? parseInt(req.query.limit) : 50;
            req.query.offset = parseInt(req.query.offset) ? parseInt(req.query.offset) : 0;

            return next();
        };

        this.validators = require('./../validators/' + version);
    }

    createToken (id, key) {
        let tokenParams = {};
        tokenParams.createTime = Date.now();
        tokenParams.id = id;

        return jwt.sign(tokenParams, key);
    }

}

module.exports = Controller;
