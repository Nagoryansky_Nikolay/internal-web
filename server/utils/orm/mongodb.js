'use strict';
const config = require('../../../config');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

class MongoSchema {
    constructor (schemaName, options, extOptions) {
        let opt = this._parseOptions(options);
        // extOptions = this._parseExtOptions(extOptions);
        this.db;
        let schema = new mongoose.Schema(opt, extOptions);
        this._model = this.db.model(schemaName, schema);
    }

    types() {
        /* describe allowed types */
        return {
            string: mongoose.Schema.Types.String,
            integer: mongoose.Schema.Types.Number,
            date: mongoose.Schema.Types.Date,
        };
    }

    connect(db){
        this.db = mongoose.createConnection(db);
    }

    _parseExtOptions(extOptions){
        let res = {};
        if (extOptions.timestamps){
            res.timestamps = {
                createdAt: 'created_at',
                updatedAt: 'updated_at'}
        }
        return res
    }

    _parseOptions(options){
        let tmpOpt = {};
        let tmpField = {};

        for (let field in options){

            if (options[field].ref){
                tmpField.type = mongoose.Schema.Types.ObjectId;
                tmpField.ref = options[field].ref.model;
            }else{
                tmpField.type = options[field].type
            }
            if ( typeof(options[field].required) === 'boolean' ){
                tmpField.required = options[field].required
            }

            tmpOpt[field] = tmpField;
            tmpField = {};
        }
        return tmpOpt;
    }

    findAll(where, limit, offset, order){
        return this._model.find(where).limit(parseInt(limit)).skip(parseInt(offset)).sort(order);
    }

    findOne(where){
        return this._model
            .findOne(where)
    }

    count(where){
        return this._model
            .count(where)
    }

    findById(id){
        return this._model
            .findOne({_id: id})
    }

    create(value){
        return this._model.create(value);
    }

}

module.exports = MongoSchema;