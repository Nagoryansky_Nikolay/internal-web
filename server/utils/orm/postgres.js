'use strict';
const Sequelize = require('sequelize');

class PostgreSQLSchema {
    constructor(schemaName, options, extOptions) {
        /* describe postgresql schema */
        let opt = this._parseOptions(options);
        this.db;
        this._model = this.db.define(schemaName, opt, extOptions);
        this._model.sync();
    }

    types() {
        /* describe allowed types */
        return {
            string: Sequelize.TEXT,
            integer: Sequelize.INTEGER,
            date: Sequelize.DATE
        };
    }

    connect(db) {
        this.db = new Sequelize(db.dbname, db.username, db.password, {
            host: db.host,
            dialect: db.dialect,
            pool: {
                max: db.maxConnections,
                min: 0,
                idle: db.delayBeforeReconnect
            },
        });
    }


    _parseOptions(options) {
        let tmpOpt = {}, tmpField = {};

        for (let field in options) {
            tmpField.type = options[field].type;
            if (options[field].ref) {
                tmpField.references = {};
                tmpField.references.model = options[field].ref.model;
                tmpField.references.key = options[field].ref.key;
            }
            if (typeof(options[field].required) === 'boolean') {
                tmpField.required = options[field].required
            }
            if (options[field].field) {
                tmpField.field = options[field].field
            }
            tmpOpt[field] = tmpField;
            tmpField = {};
        }
        return tmpOpt;
    }

    _parseWhere(where) {
        let res = '', operator, first = true;
        if (!where) {
            return res;
        }
        for (let k in where) {
            operator = (!first) ? ' AND ' : '';
            if (k === '$or') {
                res += operator;
                res += '(';
                for (let i = 0; i < where[k].length; i++) {
                    operator = (i !== where[k].length - 1 ) ? ' OR ' : '';
                    let tmp = `${this._parseWhere(where[k][i])}${operator}`;
                    res += tmp;
                }
                res += ')';
                continue;
            }

            operator = (!first) ? ' AND ' : '';
            if (!isNaN(where[k]['$gt'])) {
                res += `${'AND'}\"${k}\">${where[k]['$gt']}`;
            }else {
                res += `${operator}\"${k}\"=${where[k]}`;
            }

            first = false;
        }
        return `(${res})`;
    }

    _parseOrder(order){
        let orderString = '';
        for (let k in order){
            orderString += ` "${k}" ${order[k]},`
        }
        return orderString.slice(0, orderString.length-1)
    }

    findAll(where, limit, offset, order ) {
        let query = `SELECT * FROM ${this._model.name} `;
        if (where) {
            query += ` WHERE ${this._parseWhere(where)} `
        }
        if (order) {
            query += ` ORDER BY ${this._parseOrder(order)} `
        }
        if (offset) {
            query += ` OFFSET ${offset} `
        }
        if (limit) {
            query += ` LIMIT ${limit} `
        }
        query += ';';
        return this.db.query(query, {type: this.db.QueryTypes.SELECT});
    }

    findOne(where) {
        let query = `SELECT * FROM ${this._model.name} `;
        if (where) {
            query += ` WHERE ${this._parseWhere(where)} `
        }
        query += ` LIMIT 1;`;
        return this.db.query(query, {type: this.db.QueryTypes.SELECT})
            .then(obj => {
                return obj[0]
            });
    }

    count(where){
        let query = `SELECT COUNT(1) FROM ${this._model.name} `;
        if (where) {
            query += ` WHERE ${this._parseWhere(where)} `
        }
        return this.db.query(query, {type: this.db.QueryTypes.SELECT})
            .then(count => {
                return parseInt(count[0].count)
            });
    }

    findById(id) {
        let query = `SELECT * FROM ${this._model.name} WHERE id=${id} ;`;
        return this.db.query(query, {type: this.db.QueryTypes.SELECT})
            .then(obj => {
            return obj[0]
        });
    }

    update(where, value){
        value = value || {};
        value.updatedAt = new Date().toISOString();
        let params = ' ';
        for(let key in value){
            params += `"${key}"='${value[key]}',`;
        }
        params = params.slice(0, params.length-1) + ' ';
        let query = `UPDATE "${this._model.name}" SET ${params} WHERE ${this._parseWhere(where)} RETURNING *;`;

        return this.db.query(query, this.db.QueryTypes.UPDATE)
    }

    create(value) {
        value.createdAt = new Date().toISOString();
        value.updatedAt = new Date().toISOString();
        let columns = Object.keys(value).map(key => {
            return `"${key}"`
        }).join(', ');
        let values = Object.keys(value).map(key => {
            return `'${value[key]}'`
        }).join(', ');
        let query = `INSERT INTO  "${this._model.name}" (${columns}) VALUES (${values}) RETURNING *;`;

        return this.db.query(query, this.db.QueryTypes.INSERT)
            .then(obj => {
            return obj[0]
        });
    }
}

module.exports = PostgreSQLSchema;