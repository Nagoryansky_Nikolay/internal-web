'use strict';
const MongoSchema = require('./mongodb');
const PostgreSQLSchema = require('./postgres');
const config = require('../../../config');

class Orm {
    constructor(dbType) {
        switch(dbType) {
            case Orm.dbTypes().mongodb:
                this._Schema = MongoSchema;
                this._Schema.prototype.connect(config.chat.mongoURI);
                break;
            case Orm.dbTypes().postgresql:
                this._Schema = PostgreSQLSchema;
                this._Schema.prototype.connect(config.chat.db);
                break;
            default:
                throw new Error('Unknown DB Type');
                break;
        }
    }

    static dbTypes() {
        return {
            mongodb: 0,
            postgresql: 1
        };
    }

    types() {
        return this._Schema.prototype.types();
    }

    defineSchema(schemaName, options, extOptions) {
        return new this._Schema(schemaName, options, extOptions);
    }
}

module.exports = Orm;