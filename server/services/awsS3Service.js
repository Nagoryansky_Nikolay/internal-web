/**
 * Created by nagoryansky_nikolay_cr on 06.01.17.
 */
'use strict';
const config = require('../../config');
const AWS = require('aws-sdk');

class S3Service {

    constructor() {
        AWS.config.region = config.s3.region;
        AWS.config.accessKeyId = config.s3.accessKeyId;
        AWS.config.secretAccessKey = config.s3.secretAccessKey;

        this.s3 = new AWS.S3({ apiVersion: '2006-03-01' });
    }

    uploadFile(key, body, contentType, contentEncoding, acl ) {
        let params = {
            Bucket: config.s3.bucketName,
            Key: key,
            Body: body,
            ContentEncoding: contentEncoding,
            ContentType: contentType,
            ACL: acl
        };

        return this.s3.putObject(params).promise();
    }

    deleteFile(key) {
        let params = {
            Bucket: config.s3.bucketName,
            Key: key
        };

        return this.s3.deleteObject(params).promise();
    }


}

module.exports = new S3Service();