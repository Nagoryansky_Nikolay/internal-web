'use strict';
const Models = require('../models/v1');
const jwt = require('jsonwebtoken');
const config = require('../../config');

class AuthService {

    constructor() {

        this.decodeToken = this._decodeToken.bind(this);
        this.auth = this._auth.bind(this);
    }
    _decodeToken (token, key){
        let data;
        try {
            data = jwt.verify(token, key);
        } catch (err) {
            data = null;
        }

        return data
    }

    _auth (req, res, next) {
        const token = req.get('x-session-token');
        if (!token){
            return res.status(401)
                .send({message: 'Unauthorized'});
        }
        let data = this.decodeToken(token, config.jwt.user.jwtKey);
        if (!data){
            return res.status(401)
                .send({message: 'Unauthorized'});
        }

        Models.users.findById(data.id)
            .then(user => {
                if (!user) {
                    return res.status(404)
                        .send({message: 'User not found'});
                }
                res.locals.authUser = user;
                next();
            })
            .catch(next);
    }


}

module.exports = new AuthService();