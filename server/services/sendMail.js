'use strict';
const nodemailer = require('nodemailer');
const config = require('../../config');

class SendMail {

    constructor () {
        this.transporter = nodemailer.createTransport({
            service: config.mail.service,
            auth: {
                user: config.mail.auth.user,
                pass: config.mail.auth.pass
            }
        });
    }

    send(mailOptions) {
        return this.transporter.sendMail(mailOptions);
    }

    getConfirmMessage(name, url) {
        return `
            <p>Hey, ${name}!</p>
            <p>You are receiving this email because you have registered at Internal-web project.</p>
            <p>Please click on the following link, or paste this into your browser 
               to complete the registration process:</p>
               <b><a href="${url}">Follow this link</a> </b>
            <p>If you did not request this, please ignore this email.</p> `;
    }

    getRestoreMessage(name, url) {
        return `
            <p>Hey, ${name}!</p>
            <p>You are receiving this email because you (or someone else) have requested the reset of the password for your account.</p>
            <p>Please click on the following link, or paste this into your browser to complete the process: </p>
                <a href="${url}">Follow this link</a>
            <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>`;
    }
}

module.exports = new SendMail();