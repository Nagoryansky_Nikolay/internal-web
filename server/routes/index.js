'use strict';

const express = require('express');
const router = express.Router();
const controllers = require('../controllers');
const config = require('../../config');
const authService = require('../services/authService');


router.route('/:version/users')
    .post(controllers.callAction('users.addUser'));

router.route('/:version/login')
    .post(controllers.callAction('users.login'));

router.route('/:version/confirm/:token')
    .get(controllers.callAction('users.confirmMail'));


router.route('/:version/restore-request/')
    .post(controllers.callAction('users.restoreRequest'));

router.route('/:version/restore/:token')
    .post(controllers.callAction('users.restorePassword'));

router.route('/:version/check-token')
    .get(controllers.callAction('users.checkUserToken'));


router.use(authService.auth);

router.route('/:version/users/:id')
    .get(controllers.callAction('users.getById'))
    .put(controllers.callAction('users.updateUser'));

router.route('/:version/users/:id/avatar')
    .delete(controllers.callAction('users.removeAvatar'))
    .post(controllers.callAction('users.uploadUserAvatar'));

router.route('/:version/users/:id/cover')
    .delete(controllers.callAction('users.removeCover'))
    .post(controllers.callAction('users.uploadUserCover'));

router.route('/:version/users')
    .get(controllers.callAction('users.getUsers'));

router.route('/:version/users/:id/friends')
    .get(controllers.callAction('users.getFriends'))
    .post(controllers.callAction('users.addFriend'));

router.route('/:version/users/:id/friends/remove')
    .post(controllers.callAction('users.removeFriend'));

router.route('/:version/users/:id/friends/accept')
    .post(controllers.callAction('users.acceptFriend'));

router.route('/:version/chats/')
    .get(controllers.callAction('chats.getAllChats'))

router.route('/:version/chats/:id/')
    .get(controllers.callAction('chats.getAllMessages'));

router.route('/:version/events/')
    .get(controllers.callAction('events.getAllEvents'))
    .post(controllers.callAction('events.addEvent'))
    .delete(controllers.callAction('events.removeEvent'));

router.route('/:version/feed/')
    .get(controllers.callAction('events.getFeed'));

module.exports = router;