'use strict';

module.exports = function () {
    function base(data) {
        function eventPhotosStructure(item) {
            return {
                photo: item.photoUrl,
            }
        }

        // when data - array with models
        if (Array.isArray(data)) {
            let result = data.map(function (photo) {
                return eventPhotosStructure(photo);
            });
            return result;
        }

        return eventPhotosStructure(data);
    }

    return {
        base: base
    }
};
