'use strict';

module.exports = function(sequelize, DataTypes){
    return {
        eventId: {
            type: DataTypes.INTEGER,
            field: 'event_id',
            references: {
                model: 'events',
                key: 'id'
            }
        },
        photo: {
            field: 'photo',
            type: DataTypes.STRING,
        }
    }
};
