"use strict";
const config = require('../../../../config');

module.exports = {
    classMethods: {
        associate: function (models) {
            this.belongsTo(models.events);
        },

    },
    getterMethods: {
        photoUrl: function () {
            if (this.photo && this.photo.startsWith('http')) {
                return this.photo;
            }
            return config.s3.bucketUrl + config.s3.EVENTS_PATH  + this.photo;
        },


    },


};
