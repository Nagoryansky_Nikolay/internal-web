'use strict';

var fs        = require('fs');
var path      = require('path');
var sequelize = require('../../utils/sequelize');
var config    = require('../../../config');
var db        = {};

fs
    .readdirSync(__dirname)
    .filter(function(file) {
        return fs.statSync(path.join(__dirname, file)).isDirectory();
    })
    .forEach(function(file) {

        let pathToModel = path.join(__dirname, file);
        let methods = require(pathToModel + '/methods');
        let format = require(pathToModel + '/format');
        let schema = require(pathToModel + '/schema');

        methods.classMethods.__defineGetter__('format', format);

        let model = sequelize.importCache[path] = sequelize.define(file, schema(sequelize, sequelize.Sequelize), methods);

        db[model.name] = model;
    });

Object.keys(db).forEach(function(modelName) {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
    db['sequelize'] = sequelize;
});

module.exports = db;
