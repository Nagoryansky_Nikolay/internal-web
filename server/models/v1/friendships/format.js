'use strict';

module.exports = function () {
    function base(data) {
        function friendshipsStructure(item) {
            item = item.dataValues;
            return {
                id: item.id,
                user_id: item.user_id || '',
                friend_id: item.friend_id || '',
                status: item.status,
            }
        }

        // when data - array with models
        if (Array.isArray(data)) {
            let result = data.map(function (user) {
                return friendshipsStructure(user);
            });

            return result;
        }

        return friendshipsStructure(data);
    }

    return {
        base: base
    }
};
