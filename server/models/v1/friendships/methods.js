"use strict";


module.exports = {
    classMethods: {
        associate: function (models) {
            this.belongsTo(this, {
                through: models.users,
                foreignKey: 'userId',
                as: 'user'
            });
        },

        STATUSES: function() {
            return {
                NULL: null,
                FRIEND: 0,
                FOLLOWER: 1,
                FOLLOWEE: 2
            };
        }


        // hooks: [{
        //     name: 'afterCreate',
        //     function: function(Models) {
        //         return function(friendship, options) {
        //             console.log(friendship);
        //             return Models.friendships.create({
        //                 userId:friendship.friendId,
        //                 friendId:friendship.friendId,
        //                 status: 1
        //             })
        //                 .then(function (friendship) {
        //                     console.log(friendship)
        //                     return friendship;
        //                 })
        //         }
        //     },
            // afterCreate: function(friendship, options) {
            //     console.log(friendship);
            //     Models.friendships.findById(friendship.userId)
            //     //     .then(function (friendship) {
            //     //         if (!friendship) {
            //     //             console.log(friendship)
            //     //         }
            //     //         return res.send(Models.users.format.base(user));
            //     //     })
            //     //     .catch(next);
        //     // },
        // }]
    },
    // indexes: [
    //     {
    //         unique: true,
    //         fields: ['user_id', 'friend_id']
    //     }
    //     ]


};
