'use strict';

module.exports = function(sequelize, DataTypes){
    return {
        userId: {
            type: DataTypes.INTEGER,
            field: 'user_id',
            references: {
                model: 'users',
                key: 'id'
            }
        },

        friendId: {
            type: DataTypes.INTEGER,
            field: 'friend_id',
            references: {
                model: 'users',
                key: 'id'
            }
        },

        status: {
            field: 'status',
            type: DataTypes.INTEGER,
        },

    }
};
