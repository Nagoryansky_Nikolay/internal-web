'use strict';

module.exports = function(sequelize, DataTypes){
    return {
        username: {
            type: DataTypes.STRING,
            field: 'user_name'
        },
        email: {
            type: DataTypes.STRING,
        },
        password: {
            type: DataTypes.STRING
        },
        firstname: {
            type: DataTypes.STRING,
            field: 'first_name',
        },
        followersCount: {
            type: DataTypes.INTEGER,
            field: 'followers_count',
        },
        lastname: {
            type: DataTypes.STRING,
            field: 'last_name'
        },
        avatar: {
            type: DataTypes.STRING,
            field: 'avatar'
        },
        cover: {
            type: DataTypes.STRING,
            field: 'cover'
        },
        facebookId: {
            type: DataTypes.STRING,
            field: 'facebook_id'
        },
        twitterId: {
            type: DataTypes.STRING,
            field: 'twitter_id'
        },
        googleId: {
            type: DataTypes.STRING,
            field: 'google_id'
        },
        instagramId: {
            type: DataTypes.STRING,
            field: 'instagram_id'
        },
        gender: {
            type: DataTypes.BOOLEAN,
            field: 'gender'
        },
        country: {
            type: DataTypes.STRING,
            field: 'country'
        },
        city: {
            type: DataTypes.STRING,
            field: 'city'
        },
        latitude: {
            type: DataTypes.FLOAT,
            field: 'latitude'
        },
        longitude: {
            type: DataTypes.FLOAT,
            field: 'longitude'
        },
        birthday: {
            type: DataTypes.DATE,
            field: 'birthday'
        },
        isConfirmEmail: {
            type: DataTypes.BOOLEAN,
            field: 'is_confirm_email'
        },
    }
};
