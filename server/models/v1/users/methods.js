"use strict";
const config = require('../../../../config');

module.exports = {
    classMethods: {
        associate: function (models) {
            this.belongsToMany(this, {
                through: models.friendships,
                foreignKey: 'userId',
                as: 'friends'
            });

            this.belongsToMany(this, {
                through: models.friendships,
                foreignKey: 'friendId',
                as: 'followers'
            });
            this.hasMany(models.events);
        },

    },
    instanceMethods: {
        formatUser: function() {
            return {
                id: this.id,
                firstname: this.firstname || '',
                lastname: this.lastname || '',
                avatar: this.avatarUrl,
                friendshipStatus: this.friends[0].friendships ? this.friends[0].friendships.status : ''
            }}

    },
    getterMethods: {
        avatarUrl: function () {
            if (this.avatar && this.avatar.startsWith('http')) return this.avatar; //for social's avatar
            return this.avatar ?
                (config.s3.bucketUrl + config.s3.AVATARS_PATH  + this.avatar) :
                config.defaultUserFields.AVATAR_URL
        },
        smAvatarUrl: function () {
            if (this.avatar && this.avatar.startsWith('http')) return this.avatar; //for social's avatar
            return this.avatar ?
                (config.s3.bucketUrl + config.s3.SM_AVATARS_PATH  + this.avatar) :
                config.defaultUserFields.AVATAR_URL
        },

        coverUrl: function () {
            return this.cover ?
                (config.s3.bucketUrl + config.s3.COVERS_PATH + this.cover) :
                config.defaultUserFields.COVER_URL

        },
        genderValue: function () {
            if (this.gender === null ) return '';
            return this.gender ? 'Male' : 'Female';
        },

    },
    scopes: {
        getUsers: function (model, limit, offset, where ) {
            return {
                include: [{
                    model: model.users,
                    as: 'friends'
                }],
                where: where,
                order: [
                    ['followers_count', 'DESC']
                ],
                offset: offset,
                limit: limit
            }
        },

        getFriends: function (model, limit, offset, where, id, status ) {
            return {
                include: [
                    {
                        model: model.friendships,
                        where: {
                            userId: id,
                            //status: status
                        },
                        as: 'friendships',
                        attributes: ['status']
                    }
                ],
                where: where,
                order: [
                    ['followers_count', 'DESC']
                ],
                offset: offset,
                limit: limit
            }
        }
    }
};
