'use strict';

module.exports = function () {
    function base(data) {
        function userStructure(item) {
            let isSocialAccount = (item.facebookId || item.instagramId || item.twitterId || item.googleId) ? true : false;
            let res = {
                id: item.id,
                email: item.email || '',
                username: item.username || '',
                firstname: item.firstname || '',
                lastname: item.lastname || '',
                birthday: item.birthday || '',
                gender: item.genderValue,
                city: item.city || '',
                country: item.country || '',
                latitube: item.latitube || 0,
                longitube: item.longitube || 0,
                followersCount: item.followersCount,
                avatar: item.avatarUrl,
                cover: item.coverUrl,
                isSocialAccount: isSocialAccount
            };
            if (item.friends)  res.friends = item.friends.map(userStructure);
            if (item.followers)  res.followers = item.followers.map(userStructure);
            if (item.friendships) res.friendshipStatus = item.friendships.status ;//|| item.friendships[0].status ;

            return res
        }

        // when data - array with models
        if (Array.isArray(data)) {
            let result = data.map(userStructure);
            return result;
        }

        return userStructure(data);
    }

    function short(data) {
        function userStructure(item) {
            return {
                id: item.id,
                firstname: item.firstname || '',
                lastname: item.lastname || '',
                avatar: item.smAvatarUrl
            };
        }

        // when data - array with models
        if (Array.isArray(data)) {
            let result = data.map(userStructure);
            return result;
        }

        return userStructure(data);
    }

    return {
        base: base,
        short: short
    }
};
