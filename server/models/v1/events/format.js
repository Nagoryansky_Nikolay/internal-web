'use strict';

module.exports = function () {
    function base(data) {
        function eventsStructure(item) {
            let res = {
                id: item.id,
                description: item.description,
                lat: item.latitude,
                lng: item.longitude,
                createdAt: item.createdAt,
            };
            if(item.eventPhotos && item.eventPhotos.length){
                res.photos = item.getPhotos;
            }
            if(item.user){
                res.user = item.user.formatUser();
            }
            return res
        }

        // when data - array with models
        if (Array.isArray(data)) {
            let result = data.map(function (event) {
                return eventsStructure(event);
            });
            return result;
        }

        return eventsStructure(data);
    }

    return {
        base: base
    }
};
