'use strict';

module.exports = function(sequelize, DataTypes){
    return {
        userId: {
            type: DataTypes.INTEGER,
            field: 'user_id',
            references: {
                model: 'users',
                key: 'id'
            }
        },
        description: {
            field: 'description',
            type: DataTypes.STRING,
        },
        latitude: {
            type: DataTypes.FLOAT,
            field: 'latitude'
        },
        longitude: {
            type: DataTypes.FLOAT,
            field: 'longitude'
        },
        isDeleted : {
            type: DataTypes.BOOLEAN,
            field: 'is_deleted'
        }
    }
};
