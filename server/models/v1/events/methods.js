"use strict";
const config = require('../../../../config');


module.exports = {
    classMethods: {
        associate: function (models) {
            this.hasMany(models.eventPhotos);
            this.belongsTo(models.users);
        }
    },

    getterMethods: {
        getPhotos: function () {
            return this.eventPhotos.map((item) => {
                if (item.photo && item.photo.startsWith('http')) {
                    return item.photo;
                }
                return config.s3.bucketUrl + config.s3.EVENTS_PATH  + item.photo;
            });
        }
    },


};
