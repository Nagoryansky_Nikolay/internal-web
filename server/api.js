'use strict';

const express              = require('express');
const bodyParser           = require('body-parser');
const path                 = require('path');
const router               = require('./routes');
const config               = require('../config/index');

// initialize the app
const app = module.exports = express();

const ROOT                 = path.resolve(__dirname, '..');
const APP_PATH             = path.resolve(ROOT, 'frontend/build');
const NODE_MODULES_PATH    = path.resolve(ROOT, 'node_modules');

const passport             = require('passport');
const controller           = require('./utils/controller');
const auth_sn              = require('./auth_sn');
const errorHandler         = require('./utils/errorHandler');

app.use(passport.initialize());

// initialize  passport
auth_sn.facebook.init();
auth_sn.twitter.init();
auth_sn.google.init();
auth_sn.instagram.init();



app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));

app.use(bodyParser.json({limit: '25mb'}));

app.use(express.static(APP_PATH));

app.use('/node_modules', express.static(NODE_MODULES_PATH));


app.get('/auth/facebook', passport.authenticate('facebook',{ session: false }));
app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { session: false, failureRedirect: '/'}),
    auth_sn.findOrCreate,
    auth_sn.redirectToAuth);

app.get('/auth/twitter', passport.authenticate('twitter', { session: false }));
app.get('/auth/twitter/callback',
    passport.authenticate('twitter', { session: false, failureRedirect: '/'}),
    auth_sn.findOrCreate,
    auth_sn.redirectToAuth);

app.get('/auth/google', passport.authenticate('google',
    {   scope: [
            'https://www.googleapis.com/auth/plus.login',
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/userinfo.email'
        ],
        session: false  }));
app.get('/auth/google/callback',
    passport.authenticate('google', { session: false, failureRedirect: '/'}),
    auth_sn.findOrCreate,
    auth_sn.redirectToAuth);

app.get('/auth/instagram', passport.authenticate('instagram', { session: false }));
app.get('/auth/instagram/callback',
    passport.authenticate('instagram', { session: false, failureRedirect: '/'}),
    auth_sn.findOrCreate,
    auth_sn.redirectToAuth);


app.use('/api', router);



app.get('/*', function (req, res) {
    res.sendFile(APP_PATH + '/index.html');
});

// app.use(errorHandler(app));



