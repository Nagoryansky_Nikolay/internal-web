'use strict';
var Base = require('../../utils/baseValidator');
var validator = require('../../utils/bodyValidator');

class EventsValidator extends Base{

    static add (req, res, next){
        return validator({
            properties: {
                description: {
                    type: 'string',
                    minLength: 3,
                    maxLength: 255,
                    allowEmpty: false,
                    required: true
                },
                photos:{
                    type: 'array',
                    maxItems: 6,
                    allowEmpty: true,

                },
                lat: {
                    type: ['number', 'null'] ,
                    allowEmpty: true
                },
                lng: {
                    type: ['number', 'null'] ,
                    allowEmpty: true
                }
            }
        })(req, res, next);
    };


}
module.exports = EventsValidator;
