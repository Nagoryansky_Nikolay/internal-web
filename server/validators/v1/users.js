'use strict';
var Base = require('../../utils/baseValidator');
var validator = require('../../utils/bodyValidator');

class UsersValidator extends Base{

    static add (req, res, next){
        return validator({
            properties: {
                username: {
                    type: 'string',
                    minLength: 3,
                    pattern: '^[a-z0-9-]+$',
                    allowEmpty: false,
                    required: true
                },
                email: {
                    type: 'string',
                    allowEmpty: false,
                    format: 'email',
                    required: true
                },
                password: {
                    minLength: 6,
                    type: 'string',
                    pattern: '^[^\\s]+$',
                    allowEmpty: false,
                    required: true
                },
                confirm_password: {
                    minLength: 6,
                    pattern: '^[^\\s]+$',
                    type: 'string',
                    allowEmpty: false,
                    required: true
                },
                firstname: {
                    minLength: 3,
                    pattern: '^[a-zA-Z]+$',
                    type: 'string',
                    allowEmpty: false,
                    required: true
                },
                lastname: {
                    minLength: 3,
                    pattern: '^[a-zA-Z]+$',
                    type: 'string',
                    allowEmpty: false,
                    required: true
                },
            }
        })(req, res, next);
    };

    static update (req, res, next){
        return validator({
            properties: {
                username: {
                    type: 'string',
                    minLength: 3,
                    pattern: '^[a-z0-9-]+$',
                    allowEmpty: false,
                    required: true
                },
                email: {
                    type: 'string',
                    allowEmpty: false,
                    format: 'email',
                    required: true
                },
                password: {
                    minLength: 6,
                    type: 'string',
                    pattern: '^[^\\s]+$',
                    allowEmpty: true,

                },
                new_password: {
                    minLength: 6,
                    type: 'string',
                    pattern: '^[^\\s]+$',
                    allowEmpty: true,

                },
                firstname: {
                    minLength: 3,
                    pattern: '^[a-zA-Z]+$',
                    type: 'string',
                    allowEmpty: false,
                    required: true
                },
                lastname: {
                    minLength: 3,
                    pattern: '^[a-zA-Z]+$',
                    type: 'string',
                    allowEmpty: false,
                    required: true
                },
            }
        })(req, res, next);
    };

    static login (req, res, next){
        return validator({
            properties: {
                email: {
                    type: 'string',
                    allowEmpty: false,
                    format: 'email',
                    required: true
                },
                password: {
                    minLength: 6,
                    type: 'string',
                    pattern: '^[^\\s]+$',
                    allowEmpty: false,
                    required: true
                }
            }
        })(req, res, next);
    };

    static restoreRequest (req, res, next){
        return validator({
            properties: {
                email: {
                    type: 'string',
                    allowEmpty: false,
                    format: 'email',
                    required: true
                }
            }
        })(req, res, next);
    };

    static restorePassword (req, res, next){
        return validator({
            properties: {
                email: {
                    type: 'string',
                    allowEmpty: false,
                    format: 'email',
                    required: true
                },
                password: {
                    minLength: 6,
                    type: 'string',
                    pattern: '^[^\\s]+$',
                    allowEmpty: false,
                    required: true
                },
                confirm_password: {
                    minLength: 6,
                    pattern: '^[^\\s]+$',
                    type: 'string',
                    allowEmpty: false,
                    required: true
                }
            }
        })(req, res, next);
    };

    static queryParams (req, res, next){
        return validator({
            properties: {
                limit: {
                    type: 'string',
                    pattern:/^\d*$/,
                    messages: {
                        pattern: 'Limit must be integer',
                    }
                },
                offset: {
                    pattern:/^\d*$/,
                    messages: {
                        pattern: 'Offset must be integer',
                    }
                },
                name: {
                    minLength: 3,
                    pattern: /^[a-zA-Z]{3,}\s*[a-zA-Z]*$/,
                    type: 'string',
                },
                min_age: {
                    pattern:/^\d*$/,
                    messages: {
                        pattern: 'Min age must be integer',
                    }
                },
                max_age: {
                    pattern:/^\d*$/,
                    messages: {
                        pattern: 'Max age must be integer',
                    }
                },

            }
        })(req, res, next);
    };
}
module.exports = UsersValidator;
