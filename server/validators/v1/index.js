'use strict';

module.exports = {
    users : require('./users'),
    events : require('./events')
};