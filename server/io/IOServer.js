'use strict';

const socketIO = require('socket.io');
const redisIO = require('socket.io-redis');
const Models  = require('../models/v1');
const ChatModels = require('../models_chat/v1');

const jwt = require('jsonwebtoken');
const config = require('./../../config');


let _instance;
/**
 * class IOServer
 */
class IOServer {
    /**
     * constructor function for initialization io instance, and initialization all channels and utils
     * @param server {object} http server
     * @returns {IOServer|*}
     */
    constructor(server) {
        //singleton
        if(_instance){
            return _instance;
        }
        const IORouter = require('./IORouter');

        if(!server){
            throw new TypeError
        }
        //up io server
        _instance = this._instance = socketIO.listen(server, {
            pingTimeout: 4000,
            pingInterval: 4000,
            clusterSticky: true
        });

        this._instance.adapter(redisIO({
            host: 'localhost',
            port: 6379,
            key: 'io'
        }));

        console.log("IO server started");

        this._instance.use(this._auth);

        /**
         * lister for socket connection
         */
        this._instance.on('connection', function (socket) {
            //set user to own room
            socket.join('user:' + socket.user.id);

            let router = new IORouter(socket);
            router.initListeners();
        });

    }

    static getInstance () {
        return new IOServer(null);
    }

    /**
     * user authentication
     * @param socket
     * @param next
     * @private
     */
    _auth(socket, next){
        var handshakeData = socket.request;
        var token = handshakeData._query.token;
        if (!handshakeData._query.token) {
            console.log('error', 'connection declined:', {
                "ip": socket.conn.remoteAddress,
                "socket.id": socket.id,
                "reason": "missed accessToken"
            });
            socket.emit('err', {message: "Missed access token"});
            return socket.disconnect();
        }
        let data;
        try {
            data = jwt.verify(token, config.jwt.user.jwtKey);
        } catch (err) {
            socket.emit('err', {message: "Invalid access token"});
            return socket.disconnect();
        }
        Models.users.findById(data.id)
            .then(user => {
                if (!user) {
                    socket.emit('err', {message: "User not found"});
                    return socket.disconnect();
                }
                socket.user = user;
                next();
            })
            .catch(next);
    }



}

module.exports = IOServer;