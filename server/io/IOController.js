'use strict';
const utils = require('./IOUtils');

/**
 * class IOController
 */
class IOController {
    /**
     * io base controller
     * @returns {IOController|*}
     */
    constructor(socket){
        this.socket = socket;
        this.socketUtils = utils;
    }

    /**
     * emit message, if 'to' is null send message ty self
     * @param message {object}
     * @param channel {string}
     * @param [to=null] {string}
     * @returns {boolean}
     */
    emitMessage(channel, message, to){
        
        var transport = this.socket;

        if(to){
            transport = transport.to(to);
        }
        transport.emit(channel, message);

        return false;
    }

    emitServerMessage(channel, message, to) {
        var transport = this.socket.server;

        if(to){
            transport = transport.to(to);
        }
        transport.emit(channel, message);

        return false;
    }
}

module.exports = IOController;
