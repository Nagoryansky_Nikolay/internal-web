'use strict';
const ControllerManager = require('./controllers/ControllerManager');

/**
 * class IORouter
 */
class IORouter {
    /**
     *
     * @param socket {object} current socket connection
     * @returns {IORouter|*}
     */
    constructor(socket){

        this.socket = socket;
        this.initListeners = this._addListeners;
    }

    /**
     * this method returned list with socket event name end handler
     * @returns {*[]} list of routes
     * @private
     */
    get _routList(){
        return [
            {
                channel: 'message',
                handler: 'messages.onMessage'
            },
            {
                channel: 'typingMessage',
                handler: 'messages.onTypingMessage'
            },

        ];
    }

    /**
     * init all listeners
     * @private
     */
    _addListeners(routList){
        if(!routList){
            routList = this._routList;
        }

        let self = this;
        var controllers = new ControllerManager({}, this.socket);

        routList.forEach(function(route){
            self.socket.on(route.channel, controllers.callAction(route.handler, 'v1'))
        });
    }
}

module.exports = IORouter;
