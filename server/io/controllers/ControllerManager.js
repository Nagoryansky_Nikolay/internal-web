'use strict';

const async = require('async');
const fs = require('fs');
const path = require('path');

class ControllerManager {
    constructor (versions, socket) {
        this.versions = versions || {};
        this.socket = socket;

        fs.readdirSync(__dirname)
            .filter(function(file) {
                return fs.statSync(path.join(__dirname, file)).isDirectory();
            })
            .forEach((item) => {
                this.versions[item] = {};
                fs.readdirSync(__dirname + '/' + item)
                    .forEach((controller) => {
                        versions[item][controller.split('Controller')[0]] = this._getController.apply(this, [controller.split('.')[0], item])
                    })
            });
    }

    /**
     * requires a controller module from sub-directory
     * @param controller {string}
     * @param version {string}
     * @private
     */
    _getController (controller, version) {
        var Controller = require('./' + version + '/' + controller);
        return new Controller(version, this.socket);
    }

    /**
     * calls specified method of controller
     * @param route {string}
     * @param version {string}
     * @returns {Function}
     */
    callAction (route, version) {
        return function (data) {
            var controller = route.split('.')[0];
            var action = route.split('.')[1];

            var operations = Array.isArray(this.versions[version][controller][action])
                ? this.versions[version][controller][action]
                : [this.versions[version][controller][action]];
            operations = operations.map((middleware) => {
                return middleware.bind(this.versions[version][controller], data);
            });

            async.series(operations, function(){
                //TODO: set logger here
            });
        }.bind(this)
    }
}

module.exports = ControllerManager;
