'use strict';
const baseController = require('./../../IOController');
const Models = require('../../../models/v1');
const chatModels = require('../../../models_chat/v1');

/**
 * class messageController
 */
class messageController extends baseController{
    constructor(version, socket){
        super(socket);
        this.onMessage = [
            this._checkAccess,
            this._checkChat,
            this._checkMessage,
            this._saveMessage,
            this._sendMessage
        ];
        this.onTypingMessage = [
            this._checkChat,
            this._onTypingMessage
        ];
    }

    _checkChat(data, cb){
        chatModels.chats.findById(data.chatId)
            .then(chat => {
                if (chat.member1 == this.socket.user.id || chat.member2 == this.socket.user.id){
                    this.socket.chat = chat;
                    this.socket.uid = (chat.member1 === this.socket.user.id) ? chat.member2 : chat.member1;
                    cb()
                }else{
                    this.emitMessage('err', {message: "Chat does not exist"});
                }
            })
            .catch();
    }

    _checkAccess(data, cb){
        Models.friendships.findOne({
            where: {
                userId: this.socket.user.id,
                friendId: this.socket.uid
            }
        })
            .then(friendship => {
                if (friendship && friendship.status == Models.friendships.STATUSES().FRIEND){
                    cb()
                }else{
                    this.emitMessage('err', {message: "Access denied. You can't send messages to user"});
                }
            })
            .catch();
    }

    _checkMessage(data, cb){
        if (!data.text || !data.chatId){
            return
        }
        cb()
    }

    _saveMessage(data, cb){
        chatModels.messages.create({
            text: data.text,
            senderId: this.socket.user.id,
            chatId: data.chatId
        })
            .then( message => {
                this.socket.message = message;
                chatModels.chats.update({id: data.chatId}, {countMessages: this.socket.chat.countMessages + 1})
                    .then(() => cb());
                })
            .catch(err => {
                this.emitMessage('err', {message: "Error! Message is not delivered"});
            });

    }

    _sendMessage(data, cb) {
        const params = {
            text: this.socket.message.text,
            senderId: this.socket.message.senderId,
            chatId: this.socket.message.chatId,
            createdAt:  this.socket.message.createdAt,
            updatedAt:  this.socket.message.updatedAt,
            user: {
                id: this.socket.user.id,
                firstname: this.socket.user.firstname,
                lastname: this.socket.user.lastname,
            }
        };
        this.emitMessage('message', params, 'user:' + this.socket.uid);
        this.emitMessage('message', params);
        cb();
    }

    _onTypingMessage(data, cb){
        this.emitMessage('typingMessage', {}, 'user:' + this.socket.uid);
        cb();
    }
}


module.exports = messageController;


