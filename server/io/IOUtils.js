'use strict';
const IOServer = require('./IOServer');
const server = IOServer.getInstance();

/**
 * class ioUtils
 */
class IOUtils {
    /**
     *
     * @param ioInstance {object} current io instance
     * @returns {IoUtils|*}
     */
    constructor(ioInstance){
        this.io = ioInstance;
    }

    /**
     * get count of current connected sockets
     * @returns {number}
     */
    get currentConnectionsCount(){
        return this.io.engine.clientsCount;
    }
}

module.exports = new IOUtils(server);
