const config = require('../../../config');
const fs = require('fs');
const path = require('path');
const Orm  = require('../../utils/orm/index');

// let db = new Orm(Orm.dbTypes().mongodb);
let db = new Orm(Orm.dbTypes().postgresql);

let models = {};

fs.readdirSync(__dirname)
    .filter(function(file) {
        return !fs.statSync(path.join(__dirname, file)).isDirectory();
    })
    .forEach(function(file) {
        let name = path.parse(path.join(__dirname, file)).name;
        let schema = require(`./${file}`);
        for (let opt in schema.options){
            schema.options[opt].type = db.types()[schema.options[opt].type];
        }
        models[name] =  db.defineSchema(name, schema.options, schema.extOptions);
});

module.exports = models;
