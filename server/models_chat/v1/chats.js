'use strict';

let chatsSchema = {
    name: 'chats',

    options: {
        member1: {
            type: 'integer',
            required: true
        },
        member2: {
            type: 'integer',
            required: true
        },
        countMessages: {
            type: 'integer',
            required: true
        }
    },

    extOptions: {
        timestamps: true
    }
};

module.exports = chatsSchema;