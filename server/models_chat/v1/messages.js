'use strict';

let messagesSchema = {
    name: 'messages',

    options:  {
        text: {
            type: 'string',
            required: true,
        },
        chatId: {
            type: 'integer',
            ref:{
                model: 'chats',
                key: 'id'
            },
            required: true
        },
        senderId: {
            type: 'integer',
            required: true
        }
    },

    extOptions: {
        timestamps: true
    }
};

module.exports = messagesSchema;

