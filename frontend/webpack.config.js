const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    entry: path.resolve(__dirname, 'app', 'app.js'),
    output: {
        path: path.join(__dirname, 'build'),
        filename: 'app.js'
    },
    devtool: 'source-map',
    module: {
        loaders: [
            {test: /\.js$/, loader: 'babel?presets=es2015', exclude: /node_modules/},
            {test: /\.html$/, loader: 'html'},
            {test: /\.css$/, loader: ExtractTextPlugin.extract('style', 'css')},
            {test: /\.scss$/, loader: ExtractTextPlugin.extract('style', 'css!sass?sourceMap')},
            {test: /\.(jpe?g|png|gif|svg|woff|woff(2)?|eot|ttf)$/i, loader: 'file?name=assets/[path][name].[ext]&context=frontend/app'}
        ]
    },
    devServer: {
        port: 8088
    },
    plugins: [
        new ExtractTextPlugin('bundle.css'),
        new HtmlWebpackPlugin({
            template: 'frontend/app/index.html'
        })
    ]
};