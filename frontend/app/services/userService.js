export class UserService {

    constructor($http) {
        'ngInject';
        this.$http = $http;
    }

    updateUser(user) {
        return this.$http({
            url: '/api/v1/users/me',
            data: JSON.stringify(user),
            method: 'PUT'
        })
    }

    getCurrentUser() {
        return this.$http({
            url: '/api/v1/users/me',
            method: 'GET',
        })
    }

    getUserById(id) {
        return this.$http({
            url: `/api/v1/users/${id}`,
            method: 'GET',
        })
    }

    removeUserAvatar(user) {
        return this.$http({
            url: '/api/v1/users/me/avatar',
            method: 'DELETE',
            data: JSON.stringify(user)
        })
    }

    removeUserCover(user) {
        return this.$http({
            url: '/api/v1/users/me/cover',
            method: 'DELETE',
            data: JSON.stringify(user)
        })
    }

    uploadUserAvatar(file, filename, user) {
        return this.$http({
            url: '/api/v1/users/me/avatar',
            method: 'POST',
            data: {
                'user': user,
                'file': file,
                'filename': filename
            }
        })
    }

    uploadUserCover(file, filename, user) {
        return this.$http({
            url: '/api/v1/users/me/cover',
            method: 'POST',
            data: {
                'user': user,
                'file': file,
                'filename': filename
            }
        })
    }

    getUsers(offset, limit, name, gender, min_age, max_age) {
        let params = {};
        params.offset = offset * limit;
        params.limit = limit;
        if (name) params.name = name;
        if (gender) params.gender = gender;
        if (min_age) params.min_age = min_age;
        if (max_age) params.max_age = max_age;
        return this.$http({
            url: `/api/v1/users/`,
            method: 'GET',
            params: params
        })
    }

    getUserFriends(userId, offset, limit, name) {
        let params = {};
        params.offset = offset * limit;
        params.limit = limit;
        if (name) params.name = name;
        return this.$http({
            url: `/api/v1/users/${userId}/friends`,
            method: 'GET',
            params: params
        })
    }

    addFriend(friend_id) {
        return this.$http({
            url: '/api/v1/users/me/friends',
            method: 'POST',
            data: JSON.stringify(
                {id: friend_id}
            )
        })
    }

    acceptFriend(friend_id) {
        return this.$http({
            url: '/api/v1/users/me/friends/accept',
            method: 'POST',
            data: JSON.stringify(
                {id: friend_id}
            )
        })
    }

    removeFriend(friend_id) {
        return this.$http({
            url: '/api/v1/users/me/friends/remove',
            method: 'POST',
            data: JSON.stringify(
                {id: friend_id}
            )
        })
    }

}