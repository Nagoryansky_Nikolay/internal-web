export class ChatService{

    constructor($http) {
        'ngInject';
        this.$http = $http;

    }

    getAllChats(offset, limit, uid){
        let params = {offset, limit};
        if (uid){
            params.uid = uid;
        }
        return this.$http({
            url: `/api/v1/chats/` ,
            method: 'GET',
            params: params
        })
    }

    getAllMessages(chatId, offset, limit){
        let params = {
            offset: offset,
            limit: limit
        };
        return this.$http({
            url: `/api/v1/chats/${chatId}` ,
            method: 'GET',
            params: params
        })
    }

}