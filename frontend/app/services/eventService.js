export class EventService{

    constructor($http) {
        'ngInject';
        this.$http = $http;

    }

    addEvent(event) {
        return this.$http({
            url: '/api/v1/events',
            method: 'POST',
            data: JSON.stringify(
                event
            )
        })
    }

    removeEvent(eventId) {
        return this.$http({
            url: '/api/v1/events',
            method: 'DELETE',
            data: JSON.stringify(
                {id: eventId}
            )
        })
    }

    getAllEvents(offset, limit, uid){
        let params = {
            offset: offset,
            limit: limit,
            uid: uid
        };
        return this.$http({
            url: `/api/v1/events/` ,
            method: 'GET',
            params: params
        })
    }

    getFeed(offset, limit, lat, lng){
        let params = {
            offset,
            limit,
            lat,
            lng
        };
        return this.$http({
            url: `/api/v1/feed/` ,
            method: 'GET',
            params: params
        })
    }

}