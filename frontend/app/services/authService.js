export class AuthService {

    constructor($http) {
        'ngInject';
        this.$http = $http;
        if (this.isAuth()){
            this.token = localStorage.getItem('accessToken');
            this.user = JSON.parse(localStorage.getItem('user'));
            this.$http.defaults.headers.common['x-session-token'] = this.token;
        }
    }

    setToken(token) {
        this.token = token;
        localStorage.setItem('accessToken', token);
        this.$http.defaults.headers.common['x-session-token'] = token;
    }

    setUser(user){
        this.user = user;
        localStorage.setItem('user', JSON.stringify(user));
    }

    checkToken() {
        if (this.token) {
            return Promise.resolve();
        }
        return this.$http({
            url: '/api/v1/check-token',
            method: 'GET',
        })
    }

    isAuth() {
        return localStorage.getItem('accessToken') ? true : false;
    }

    logout() {
        localStorage.clear();
    }

    login(user) {
        return this.$http({
            url: '/api/v1/login',
            data: JSON.stringify(user),
            method: 'POST',
        })
    }

    register(user) {
        return this.$http({
            url: '/api/v1/users',
            data: JSON.stringify(user),
            method: 'POST',
        })
    }

    confirmToken(token) {
        return this.$http({
            method: 'GET',
            url: '/api/v1/confirm/' + token,
        })
    }

    requestRestorePassword(user) {
        return this.$http({
            method: 'POST',
            url: '/api/v1/restore-request/',
            data: JSON.stringify(user),
        })
    }

    restorePassword(user, token) {
        return this.$http({
            method: 'POST',
            url: '/api/v1/restore/' + token,
            data: JSON.stringify(user),
        })
    }


}