export class FriendStateService{

    constructor( authService) {
        'ngInject';
        this.authService = authService;
        this.STATUSES =  {
            NONE: null,
            FRIEND: 0,
            FOLLOWER: 1,
            FOLLOWEE: 2
        };
    }

    getFriendState(status, userId){
        let states = {};
        states.TO_ADD = status===this.STATUSES.NONE&&this.authService.user.id!==userId;

        states.TO_ACCEPT = status === this.STATUSES.FOLLOWER && this.authService.user.id!== userId;

        states.TO_REMOVE = (status === this.STATUSES.FOLLOWEE || status === this.STATUSES.FRIEND)&& this.authService.user.id!==userId;
        return states;
    }
}