import io from '../../../node_modules/socket.io-client/socket.io';
export class IOService{

    constructor($rootScope) {
        'ngInject';
        this.$rootScope = $rootScope;
    }

    connect(url, token){
        this.socket = io(url + `?token=${token}`);
    }

    on(event, callback) {
         this.socket.on(event, (data) => {
             this.$rootScope.$apply(() => {
                 callback.apply(this.socket, [data]);
             });
        });
    }

    emit(event, data, callback) {
        if (typeof callback === 'function') {
            this.socket.emit(event, data, function () {
                callback.apply( this.socket, arguments);
            });
        }
        else
            this.socket.emit(event, data);
    }

}