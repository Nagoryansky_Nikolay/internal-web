export class ListItemValues{

    constructor() {
        'ngInject';
        this.genders = [
            {
                name: null,
                condition: false,
                index: 0
            },
            {
                name: 'Male',
                condition: false,
                index: 1
            },
            {
                name: 'Female',
                condition: false,
                index: 2
            }
        ];

        this.ageList = [
            {
                name: null,
                min: null,
                max: null,
                condition: false
            },
            {
                name: '18-25',
                min: 18,
                max: 25,
                condition: false
            },
            {
                name: '26-45',
                min: 26,
                max: 45,
                condition: false
            },
            {
                name: '46-60',
                min: 46,
                max: 60,
                condition: false
            },
            {
                name: '60+',
                min: 60,
                max: null,
                condition: false
            },
        ];
        this.ageList.forEach(function(item, i, arr) { item.index = i;});
    }

    getGenderList(){
        return  this.genders
    }

    getAgeList(){
        return  this.ageList
    }



}