export class NotificationService{

    constructor(ngNotify) {
        'ngInject';
        const BASE_CONFIG = {
            theme: 'pure',
            position: 'top',
            duration: 3000,
        };

        this.ngNotify = ngNotify;
        this.ngNotify.config(BASE_CONFIG);
    }

    notify(message, type){
        const DEFAULT_TYPE = 'info';
        return this.ngNotify.set(message, { type: type || DEFAULT_TYPE });
    }

    error(message) {
        return this.ngNotify.set(message, 'error');
    }

    warn(message) {
        return this.ngNotify.set(message, 'warn');
    }

}