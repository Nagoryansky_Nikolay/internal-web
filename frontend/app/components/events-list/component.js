import EventsListController from './controller';

export const EventsListComponent = {
    template: require('./events-list-template.html'),
    controller: EventsListController,
    restrict: 'E',
    bindings: {
    },
};