export default class EventsListController {
    constructor($scope, notifyService, eventService, authService, $stateParams, $uibModal) {
        this.$scope = $scope;
        this.eventService =eventService;
        this.notifyService = notifyService;
        this.authService = authService;
        this.$stateParams = $stateParams;
        this.$uibModal = $uibModal;
        this.$scope.userId = this.$stateParams.userId;
        this.$scope.isOwnerPage = this.$scope.userId == this.authService.user.id || this.$scope.userId === 'me';

        this.$scope.ST_MAP_KEY = 'AIzaSyDOhWhk-SC3Jy-fRZCnG-NbrMhf8at0ojE';
        this.$scope.events = [];
        this.$scope.nearestEvents = [];
        this.$scope.newEvent = {
            description: '',
            photos: [],
            lat: null,
            lng: null
        };
        this.$scope.placeLocation = '';
        this.$scope.LIMIT = 5;
        this.$scope.FEED_LIMIT = 200;
        this.$scope.offset = 0;
        this.$scope.state = {
            busy: false,
        };

        this.$scope.MAX_IMG_SIZE = '3MB';
        this.$scope.MIN_IMG_WIDTH = '100';
        this.$scope.MIN_IMG_HEIGHT = '100';
        this.$scope.IMG_PATTERN = "'.png,.jpg'";

        this.$scope.descriptionValidationOptions = {
            rules:{
                description: {
                    minlength: 3,
                    maxlength: 255,
                    required: true
                }
            }
        };
    }

    $onInit() {
        const input = document.getElementById('event-list--place-location');
        const autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            this.$scope.newEvent.lat = autocomplete.getPlace().geometry.location.lat();
            this.$scope.newEvent.lng = autocomplete.getPlace().geometry.location.lng();
        });

        this.$scope.$watch('placeLocation', (placeLocation)=>{
            if(!placeLocation){
                this.$scope.newEvent.lat = null;
                this.$scope.newEvent.lng = null;
            }
        });
        this.$scope.$watch('errorFile', (errorFile)=>{
            if (!errorFile) return;
            let message =  '';
            switch(errorFile.$error) {
                case 'maxSize':
                    message = `File too large ${errorFile.size / 1000000}MB: max ${this.$scope.MAX_IMG_SIZE}`;
                    break;
                case 'minWidth':
                    message = `File too small, min width is ${this.$scope.MIN_IMG_WIDTH}px`;
                    break;
                case 'minHeight':
                    message = `File too small, min height is ${this.$scope.MIN_IMG_HEIGHT}px`;
                    break;
                case 'pattern':
                    message = 'File is not image';
                    break;
            }
            this.notifyService.error(message);
        });

        this.$scope.$on('removeEvent',
            (event, eventId)=>{
                this.eventService.removeEvent(eventId)
                    .then((res) => {
                        this.$scope.events = this.$scope.events.filter((event)=>{
                            return event.id !== eventId
                        });
                        this.notifyService.notify(res.data.message);
                    })
                    .catch((response) => {
                        this.notifyService.error(response.data.message);

                    })
            });

    };

    getNextEvents(){
        if (this.$scope.state.busy)
            return;
        this.$scope.state.busy = true;
        this.eventService.getAllEvents(this.$scope.offset, this.$scope.LIMIT, this.$scope.userId)
            .then((res) => {
                this.$scope.offset += this.$scope.LIMIT;
                res.data.events.forEach((event)=> {event.map = this.getMapUrl(event.lat, event.lng)});
                this.$scope.events = this.$scope.events.concat(res.data.events);
                this.$scope.state.busy =  res.data.events.length < this.$scope.LIMIT;
            })
            .catch((err) => {
                this.notifyService.error(err.data.message);
            });
    }

    getNearestEvents(lat, lng){
        return this.eventService.getFeed(0, this.$scope.FEED_LIMIT, lat, lng )
            .then((res) => {
                res.data.events.forEach((item)=> {item.map = this.getMapUrl(item.lat, item.lng)});
                return res.data.events;
            })
            .catch((err) => {
                return err;
            });
    }


    uploadImage(image){
        if (!image) return;
        const self = this;
        let reader = new FileReader();
        reader.onload = function (evt) {
            const base64 = evt.target.result;
            self.$scope.newEvent.photos.push({
                file: base64,
                filename: image.name
            });
        };
        reader.readAsDataURL(image);
    }

    saveEvent (descriptionForm) {
        if(descriptionForm.validate()) {
            this.$scope.state.saving = true;
            this.$scope.newEvent.photos = this.$scope.newEvent.photos.slice(0,6);
            this.eventService.addEvent(this.$scope.newEvent)
                .then((res) => {
                    let event = res.data.event;
                    event.map = this.getMapUrl(event.lat, event.lng);
                    this.$scope.events.unshift(event);
                    this.notifyService.notify(res.data.message);
                })
                .catch((response) => {
                    this.notifyService.error(response.data.message);

                })
                .finally(() => {
                    this.$scope.state.saving = false;
                    this.clearEvent();
                });
        }
    };

    clearEvent(){
        this.$scope.newEvent = {
            description: '',
            photos: [],
            lat: null,
            lng: null

        };
        this.$scope.placeLocation = '';
    }
    openMapPopup() {
        const lat = this.$scope.newEvent.lat;
        const lng = this.$scope.newEvent.lng;
        if (lat === null || lng === null){
            this.notifyService.error('Input location please!');
            return
        }
        let center = { lat, lng };
        this.getNearestEvents(lat, lng)
            .then((events)=>{
                this.$uibModal.open({
                    component: 'googleMap',
                    resolve: {
                        events : () => {
                            return events;
                        },
                        center : () => {
                            return center;
                        }
                    },
                    windowClass: 'google-map-modal-window',

                }).result
                    .then((res) => {console.log('$close called');})
                    .catch((res) => {console.log('$dismiss called');});
            })
            .catch(e => console.log(e));
    }

    getMapUrl(lat, lng){
        return `https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=550x170&markers=icon:https%3A%2F%2Fs3-eu-west-1.amazonaws.com%2Finternal-web-bucket%2Fcommon%2Fic_location-2x.png%7C${lat},${lng}&key=${this.$scope.ST_MAP_KEY}`;
    }

}