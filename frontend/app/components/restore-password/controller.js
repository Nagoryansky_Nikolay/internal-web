import template from './restore-password-template.html';
export default class RestorePasswordController {
    constructor($scope, $http, $location, authService, notifyService, $state, $uibModal) {
        'ngInject';
        this.$scope = $scope;
        this.$http = $http;
        this.$state = $state;
        this.notifyService = notifyService;
        this.authService = authService;

        this.$uibModal = $uibModal;
        this.$scope.isNavCollapsed = true;
        this.$scope.isCollapsed = false;
    }

    $onInit () {
        this.$scope.isSaving= false;
        this.$uibModal.open({
            template:template,
            controller: this.restore,
            windowClass: 'restore-modal-window',
        }).result
            .then((res) => {
                console.log('$restore close called');
            })
            .catch((res) => {
                console.log('$restore dismiss called');
            });

    }

    restore ($scope, $location, notifyService, authService, $state ) {
        $scope.user = {};
        $scope.restoreValidationOptions = {
            rules:{
                email: {
                    email: true,
                    required: true
                },
                password: {
                    minlength: 6,
                    regex: '^[^\\s]+$',
                    required: true
                },
                confirm_password: {
                    minlength: 6,
                    regex: '^[^\\s]+$',
                    required: true,
                    equalTo: "#password"
                },
            },
            messages: {
                password: {
                    regex: 'Allowed any character except space',
                },
                confirm_password: {
                    equalTo: 'Must be the same as “password” field',
                }
            }
        };

        $scope.restorePassword = function(form) {
            var token = $location.search().token;
            if( form.validate() ) {
                $scope.isSaving = true;
                if (token) {
                    authService.restorePassword($scope.user, token)
                        .then((response) => {
                            $state.go('landing');
                            notifyService.notify(response.data.message);
                            $scope.isSaving = false;
                            this.$close();
                        })
                        .catch((response) => {
                            console.log(response);
                            $scope.isSaving = false;
                            notifyService.error(response.data.message);
                        });
                }
            }
        };
    }


}