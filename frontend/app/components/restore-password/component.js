import RestorePasswordController from './controller';

export const RestorePasswordComponent = {
    template: require('./restore-password-template.html'),
    controller: RestorePasswordController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        close: '&',
        dismiss: '&'
    },
};