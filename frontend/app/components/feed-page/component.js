import FeedPageController from './controller';

export const FeedPageComponent = {
    template: require('./feed-page-template.html'),
    controller: FeedPageController,
    restrict: 'E',
    bindings: {

    },
};