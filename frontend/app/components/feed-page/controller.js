export default class FeedPageController {
    constructor($scope, eventService, notifyService) {
        'ngInject';
        this.$scope = $scope;
        this.notifyService = notifyService;
        this.eventService = eventService;
        this.$scope.ST_MAP_KEY = 'AIzaSyDOhWhk-SC3Jy-fRZCnG-NbrMhf8at0ojE';
        this.$scope.events = [];
        this.$scope.LIMIT = 10;
        this.$scope.offset = 0;
        this.$scope.state = {
            busy: false,
        };

    }

    $onInit() {

    };

    getNextFeed(){
        if (this.$scope.state.busy)
            return;
        this.$scope.state.busy = true;
        this.eventService.getFeed(this.$scope.offset, this.$scope.LIMIT)
            .then((res) => {
                this.$scope.offset += this.$scope.LIMIT;
                res.data.events.forEach((event)=> {event.map = this.getMapUrl(event.lat, event.lng)});
                this.$scope.events = this.$scope.events.concat(res.data.events);
                this.$scope.state.busy =  res.data.events.length < this.$scope.LIMIT;
            })
            .catch((err) => {
                this.notifyService.error(err.data.message);
            });
    }

    getMapUrl(lat, lng){
        return `https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=550x170&markers=icon:https%3A%2F%2Fs3-eu-west-1.amazonaws.com%2Finternal-web-bucket%2Fcommon%2Fic_location-2x.png%7C${lat},${lng}&key=${this.$scope.ST_MAP_KEY}`;
    }
}