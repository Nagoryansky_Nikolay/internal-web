export default class MessageController{
    constructor($state) {
        'ngInject';
        this.$state = $state;
    }

    $onInit() {

    };

    goToProfile(id){
        this.$state.go('user.friends', {userId:id})
    }
}



