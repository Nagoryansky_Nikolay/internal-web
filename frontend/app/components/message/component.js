import MessageController from './controller';

export const MessageComponent = {
    template: require('./message-template.html'),
    controller: MessageController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        message: '=',
        user: '<',
        userId: '<'
    },
};