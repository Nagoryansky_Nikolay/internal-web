import template from './request-restore-password-template.html';
export default class RequestRestorePasswordController {
    constructor($scope, $http, notifyService, $state, $uibModal, authService) {
        'ngInject';
        this.$scope = $scope;
        this.$http = $http;
        this.$state = $state;
        this.notifyService = notifyService;
        this.authService = authService;
        this.$uibModal = $uibModal;
    }

    $onInit () {
        this.$scope.isSaving= false;
        this.$uibModal.open({
            template:template,
            controller: this.requestRestore,
            windowClass: 'request-restore-modal-window',
        }).result
            .then((res) => {
                console.log('$restore-request close called');
            })
            .catch((res) =>  {
                this.$state.go('landing');
                console.log('$restore-request dismiss called');
            });

    }

    requestRestore($scope,  notifyService, $http, $state, authService ) {
        $scope.user = {};
        $scope.requestRestoreValidationOptions = {
            rules:{
                email: {
                    email: true,
                    required: true
                }
            }
        };
        $scope.requestRestorePassword = function(form) {
            if( form.validate() ) {
                $scope.isSaving= true;
                authService.requestRestorePassword($scope.user)
                    .then((response) => {
                        $state.go('landing');
                        notifyService.notify(response.data.message);
                        $scope.isSaving= false;
                        this.$close();
                    })
                    .catch((response) => {
                        console.log(response);
                        $scope.isSaving= false;
                        notifyService.error(response.data.message);
                    });
            }
        };
    }



}