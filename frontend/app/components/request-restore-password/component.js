import RequestRestorePasswordController from './controller';

export const RequestRestorePasswordComponent = {
    template: '',
    controller: RequestRestorePasswordController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        close: '&',
        dismiss: '&'
    },
};