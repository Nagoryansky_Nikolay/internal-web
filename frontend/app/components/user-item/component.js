/**
 * Created by nagoryansky_nikolay_cr on 10.01.17.
 */
import UserItemController from './controller';

export const UserItemComponent = {
    template: require('./user-item-template.html'),
    controller: UserItemController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        user: '='
    },
};