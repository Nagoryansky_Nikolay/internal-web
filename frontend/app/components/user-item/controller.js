/**
 * Created by nagoryansky_nikolay_cr on 10.01.17.
 */
export default class UserItemController {
    constructor($state, $scope, userService, authService, notifyService, $stateParams, friendState) {
        'ngInject';
        this.$state = $state;
        this.$scope = $scope;
        this.$stateParams = $stateParams;
        this.friendState = friendState;
        this.userService = userService;
        this.authService = authService;
        this.notifyService = notifyService;
        this.$scope.userId = this.$stateParams.userId;
        this.$scope.isOwnerPage = (this.$scope.userId==this.authService.user.id&&this.$state.is('user.friends'))||
            (this.$scope.userId==='me'&&this.$state.is('user.friends'));
        this.$scope.isUsersListPage = this.$state.is('search');
        this.$scope.states = {
            TO_ADD: false,
            TO_ACCEPT: false,
            TO_REMOVE: false
        };
    }

    $onInit() {
        this.$scope.$watch('status', this._changeButtonsState.bind(this));
        this.$scope.status = this.user.friendshipStatus;
        this.$scope.user = this.user;
    };

    _changeButtonsState(){
        this.user.friendshipStatus = this.$scope.status;
        this.$scope.states = this.friendState.getFriendState(this.$scope.status, this.user.id);
    }

    goToProfile(id){
        this.$state.go('user.friends', {userId:id})
    }

    addFriend(user){
        this.userService.addFriend(user.id)
            .then((response) => {
                this.$scope.status = response.data.user.friendshipStatus;
            })
            .catch((response) => {
                this.notifyService.error(response.data.message);
            });
    }

    acceptFriend(user){
        this.userService.acceptFriend(user.id)
            .then((response) => {
                this.$scope.status = response.data.user.friendshipStatus;
            })
            .catch((response) => {
                this.notifyService.error(response.data.message);
            });
    }

    removeFriend(user){
        this.userService.removeFriend(user.id)
            .then((response) => {
                this.$scope.status = response.data.user.friendshipStatus;
            })
            .catch((response) => {
                this.notifyService.error(response.data.message);
            });
    }

}