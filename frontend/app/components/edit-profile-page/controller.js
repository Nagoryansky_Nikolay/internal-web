import DPtemplate from './datepicker-template.html';
import CropImgTemplate from './crop-img-popup-template.html';

export default class  EditProfilePageController{
    constructor($scope, $http, $uibModal, authService, $state, notifyService , moment, userService,
                DEFAULT_AVATAR, DEFAULT_COVER) {
        'ngInject';
        this.$uibModal = $uibModal;
        this.authService = authService;
        this.userService = userService;
        this.notifyService = notifyService;
        this.logout = this.authService.logout;
        this.$state = $state;
        this.$scope = $scope;
        this.$scope.DEFAULT_AVATAR = DEFAULT_AVATAR;
        this.$scope.DEFAULT_COVER = DEFAULT_COVER;
        this.$scope.MAX_IMG_SIZE = '3MB';
        this.$scope.MIN_IMG_WIDTH = '100';
        this.$scope.MIN_IMG_HEIGHT = '100';
        this.$scope.IMG_PATTERN = "'.png,.jpg'";
        this.$scope.conditions = {
            isDefaultAvatar: false,
            isDefaultCover: false,
            isShowedGenderList: false,
            isSaving: false,
        };
        this.moment = moment;
        this.$http = $http;
        this.$scope.pic = null;

        this.$scope.genders = [
            {
                name: '',
                condition: false,
                index: 0
            },
            {
                name: 'Male',
                condition: false,
                index: 1
            },
            {
                name: 'Female',
                condition: false,
                index: 2
            }
        ];
    }

    $onInit() {
        this.$scope.totalItems = 764;
        this.$scope.currentPage = 4;
        this.$scope.maxSize = 5;
        this.$scope.bigTotalItems = 175;
        this.$scope.bigCurrentPage = 1;
        this.$scope.user = {};
        this.$scope.passwords = {
            currentPassword: '',
            newPassword: '',
            confirmNewPassword: '',
        };
        this.$scope.accountValidationOptions = {
            rules:{
                username: {
                    minlength: 3,
                    regex: '^[a-z0-9-]+$',
                    required: true
                },
                email: {
                    email: true,
                    required: true
                },
                firstname: {
                    minlength: 3,
                    regex: '^[a-zA-Z]+$',
                    required: true
                },
                lastname: {
                    minlength: 3,
                    regex: '^[a-zA-Z]+$',
                    required: true
                }
            },
            messages: {
                username: {
                    regex: 'Allowed only alpha-numeric characters and dashes on lower case',
                },
            }
        };
        this.$scope.passwordValidationOptions = {
            rules:{

                current_password: {
                    isPasswordEmpty: '#new_password',
                    minlength: 6,
                    regex: '^[^\\s]+$'
                },
                new_password: {
                    isPasswordEmpty: '#current_password',
                    minlength: 6,
                    regex: '^[^\\s]+$'
                },
                confirm_new_password: {
                    minlength: 6,
                    regex: '^[^\\s]+$',
                    equalTo: '#new_password'
                }
            },
            messages: {
                current_password: {
                    regex: 'Allowed any character except space',
                },
                new_password: {
                    regex: 'Allowed any character except space',
                },
                confirm_new_password: {
                    equalTo: 'Must be the same as “password” field',
                }
            }
        };

        const input = document.getElementById('edit-profile--location');
        const autocomplete = new google.maps.places.Autocomplete(input, {types: ['(cities)']});
        google.maps.event.addListener(autocomplete, 'place_changed', () => {
            const place = autocomplete.getPlace();
            if(place.address_components){
                this.$scope.user.country = place.address_components[place.address_components.length - 1].long_name;
                this.$scope.user.city = place.address_components[0].long_name;
            }
        });

        this.userService.getCurrentUser()
            .then((response) => {
                this.$scope.user = response.data;
                this.setUserGender(this.$scope.user.gender);
                this.$scope.coverStyle = {
                    'background': `rgba(46, 31, 93, 1) url(${this.$scope.user.cover}) center no-repeat`,
                    'background-size': 'cover'
                };
                this.$scope.conditions.isDefaultAvatar = (this.$scope.user.avatar === this.$scope.DEFAULT_AVATAR) ;
                this.$scope.conditions.isDefaultCover = (this.$scope.user.cover === this.$scope.DEFAULT_COVER);
            })
            .catch((response) => {
                this.logout();
                this.notifyService.error(response.data.message);
            });



        this.$scope.$watch('errorFile', this.showImageValidationError.bind(this));
        this.$scope.$watch('errorCoverFile', this.showImageValidationError.bind(this));
    };

    showImageValidationError(errorFile){
        if (!errorFile) return;
        let err = errorFile.$error;
        let message =  '';
        switch(err) {
            case 'maxSize':
                message = `File too large ${errorFile.size / 1000000}MB: max ${this.$scope.MAX_IMG_SIZE}`;
                break;
            case 'minWidth':
                message = `File too small, min width is ${this.$scope.MIN_IMG_WIDTH}px`;
                break;
            case 'minHeight':
                message = `File too small, min height is ${this.$scope.MIN_IMG_HEIGHT}px`;
                break;
            case 'pattern':
                message = 'File is not image';
                break;
            }
        this.notifyService.error(message);
    }

    setUserGender(gender){
        switch(gender) {
            case 'Male':
                this.$scope.genders[1].condition = true;
                break;
            case 'Female':
                this.$scope.genders[2].condition = true;
                break;
            default :
                this.$scope.genders[0].condition = true;
                break;
        }
    }

    showGenderList(){
        this.$scope.conditions.isShowedGenderList = !this.$scope.conditions.isShowedGenderList;
    }

    setGender(gender){
        this.$scope.user.gender = gender.name;
        this.$scope.genders.forEach(function(item, i, arr) { item.condition = false;});
        this.$scope.genders[gender.index].condition = true;
    }

    prepareUser(){
        if (this.$scope.passwords.currentPassword){
            this.$scope.user.password = this.$scope.passwords.currentPassword;
            this.$scope.user.new_password = this.$scope.passwords.newPassword;
        }
        if (this.$scope.user.birthday){
            this.$scope.user.birthday = this.moment(this.$scope.user.birthday).format('YYYY-MM-DD');
        }
    }

    saveUserInfo (accountForm, passwordForm) {
        if( accountForm.validate() && passwordForm.validate() ) {
            this.prepareUser();
            this.$scope.conditions.isSaving= true;
            this.userService.updateUser(this.$scope.user)
                .then((response) => {
                    this.$state.go('user.friends', {userId: this.$scope.user.id});
                    this.notifyService.notify('Your profile has been successfully updated');
                })
                .catch((response) => {
                    this.notifyService.error(response.data.message);

                })
                .finally(() => {
                    this.$scope.conditions.isSaving = false;
                });
        }
    };

    removeAvatar(){
        this.userService.removeUserAvatar(this.$scope.user)
            .then((response) => {
                this.$scope.user.avatar = this.$scope.DEFAULT_AVATAR;
                this.$scope.conditions.isDefaultAvatar = true;
                this.notifyService.notify(response.data.message);
            })
            .catch((response) => {
                this.notifyService.error(response.data.message);
            });
    }

    removeCover(){
        this.userService.removeUserCover(this.$scope.user)
            .then((response) => {
                this.$scope.coverStyle = {
                    'background': `rgba(46, 31, 93, 1) url(${this.$scope.DEFAULT_COVER}) center no-repeat`,
                    'background-size': 'cover'
                };
                this.$scope.conditions.isDefaultCover = true;
                this.notifyService.notify(response.data.message);

            })
            .catch((response) => {
                this.notifyService.error(response.data.message);
            });
    }

    uploadCoverImage(image){
        if (!image) return;
        let file = image;
        const self = this;
        let reader = new FileReader();
        reader.onload = function (evt) {
            const base64 = evt.target.result;
            const imageFile = {
                file: base64,
                filename: image.name
            };
            self.userService.uploadUserCover(imageFile.file, imageFile.filename, self.$scope.user)
                .then((response) => {
                    self.$scope.user.cover = response.data.user.cover;
                    self.notifyService.notify(response.data.message);
                    self.$scope.coverStyle = {
                        'background': `rgba(46, 31, 93, 1) url(${self.$scope.user.cover}) center no-repeat`,
                        'background-size': 'cover'
                    };
                    self.$scope.conditions.isDefaultCover = false;
                    this.$close();
                })
                .catch((response) => {
                    self.notifyService.error(response.data.message);
                });
        };
        reader.readAsDataURL(file);

    }

    openCrop(avatarFile){
        if (!avatarFile) return;
        this.$scope.avatarFile = avatarFile;
        this.$uibModal.open({
            template:CropImgTemplate,
            controller: this.uploadCropImage,
            scope: this.$scope,
            windowClass: 'crop-img-modal-window',
        }).result
            .then((res) => {
                this.$scope.avatarFile = null;
            })
            .catch((res) =>  {
                this.$scope.avatarFile = null;
            });
    }

    uploadCropImage($scope,notifyService, userService){
        $scope.uploadAvatar = function(file, filename){
            userService.uploadUserAvatar(file, filename, $scope.user )
                .then((response) => {
                    $scope.user.avatar = response.data.user.avatar;
                    $scope.conditions.isDefaultAvatar = false;
                    notifyService.notify(response.data.message);
                    this.$close();
                })
                .catch((response) => {
                    notifyService.error(response.data.message);
                });
        }

    }

    openDataPicker() {
        this.$scope.popup.opened = true;
    };

    openModalDataPicker(){

        this.$uibModal.open({
            template: DPtemplate,
            controller: ($scope) => {
                $scope.dateOptions = {
                    formatYear: 'yy',
                    maxDate: new Date(),
                    minDate: new Date(1910, 0, 1),
                    startingDay: 1,
                    showWeeks: false
                };
                $scope.birthday = this.$scope.user.birthday;
                $scope.clearBirthday = () => {
                    this.$scope.user.birthday = null;
                    $scope.$dismiss();
                }
            }
        }).result
            .then(date => {
                this.$scope.user.birthday = date;
            })
            .catch(date => {
            });
    }


}




