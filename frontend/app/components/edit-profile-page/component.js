import EditProfilePageController from './controller';

export const EditProfilePageComponent = {
    template: require('./edit-profile-page-template.html'),
    controller: EditProfilePageController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        close: '&',
        dismiss: '&'
    },
};



