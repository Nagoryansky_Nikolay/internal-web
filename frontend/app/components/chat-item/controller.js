export default class ChatItemController {
    constructor($state, $scope, $stateParams) {
        'ngInject';
        this.$state = $state;
        this.$scope = $scope;
        this.$stateParams = $stateParams;

    }

    $onInit() {
    };

    goToChat(id){
        this.$state.go('chats.detail', {chatId:id})
    }

}