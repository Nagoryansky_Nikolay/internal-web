import ChatItemController from './controller';

export const ChatItemComponent = {
    template: require('./chat-item-template.html'),
    controller: ChatItemController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        chat: '<'
    },
};