import LandingPageController from './controller';

export const LandingPageComponent = {
    template: require('./landing-page-template.html'),
    controller: LandingPageController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
    }

};

