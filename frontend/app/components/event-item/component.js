import EventItemController from './controller';

export const EventItemComponent = {
    template: require('./event-item-template.html'),
    controller: EventItemController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        event: '<'
    },
};