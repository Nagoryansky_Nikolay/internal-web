import GalleryPopupTemplate from './gallery-popup-template.html';
export default class EventItemController {
    constructor($scope, $stateParams, authService, $state, chatService, notifyService, eventService, $uibModal ) {
        this.$scope = $scope;
        this.$state = $state;
        this.chatService = chatService;
        this.authService = authService;
        this.eventService = eventService;
        this.$stateParams = $stateParams;
        this.$uibModal = $uibModal;
        this.notifyService = notifyService;
        this.$scope.userId = this.$stateParams.userId;
        this.$scope.isFeedPage = this.$state.is('feed');
        this.$scope.isOwnerPage = this.$scope.userId == this.authService.user.id || this.$scope.userId === 'me';
        this.$scope.FEED_LIMIT = 200;
        this.$scope.ST_MAP_KEY = 'AIzaSyDOhWhk-SC3Jy-fRZCnG-NbrMhf8at0ojE';
    }

    $onInit() {
    };

    $onChanges(changesObj) {
        if (changesObj.event.currentValue && changesObj.event.currentValue.photos) {
            this.$scope.photos = changesObj.event.currentValue.photos;
            this.$scope.photos1 = changesObj.event.currentValue.photos.slice(0,3);
            this.$scope.photos2 = changesObj.event.currentValue.photos.slice(3,6);
        }
    };

    removeEvent(){
        this.$scope.$emit('removeEvent', this.event.id);
    }

    goToProfile(id){
        this.$state.go('user.friends', {userId:id})
    }

    goToChat(user){
        let limit = 1;
        let offset = 0;
        if (user.friendshipStatus != 0){
            this.notifyService.error(`${user.firstname} is not accepted you as a friend.`);
            return;
        }
        this.chatService.getAllChats(offset, limit,  user.id)
            .then(res => {
                this.$state.go('chats.detail', {chatId: res.data.chats[0].id})
            });
    }

    getNearestEvents(lat, lng){
        return this.eventService.getFeed(0, this.$scope.FEED_LIMIT, lat, lng )
            .then((res) => {
                res.data.events.forEach((item)=> {item.map = this.getMapUrl(item.lat, item.lng)});
                return res.data.events;
            })
            .catch((err) => {
                return err;
            });
    }

    openMapPopup(lat, lng) {
        let center = { lat, lng };
        this.getNearestEvents(lat, lng)
            .then((events)=>{
                this.$uibModal.open({
                    component: 'googleMap',
                    resolve: {
                        events : () => {
                            return events;
                        },
                        center : () => {
                            return center;
                        }
                    },
                    windowClass: 'google-map-modal-window',

                }).result
                    .then((res) => {console.log('$close called');})
                    .catch((res) => {console.log('$dismiss called');});
            })
            .catch(e => console.log(e));
    }

    openGalleryPopup(index){

        this.$uibModal.open({
            template: GalleryPopupTemplate,
            windowClass: 'gallery-modal-window',
            controller: ($scope) => {
                $scope.eventImages = this.$scope.photos;
                $scope.eventDescription = this.event.description;
                $scope.imageIndex = index;
            }
        }).result
            .then(data => {
            })
            .catch(err => {
            });
    }

    getMapUrl(lat, lng){
        return `https://maps.googleapis.com/maps/api/staticmap?zoom=13&size=550x170&markers=icon:https%3A%2F%2Fs3-eu-west-1.amazonaws.com%2Finternal-web-bucket%2Fcommon%2Fic_location-2x.png%7C${lat},${lng}&key=${this.$scope.ST_MAP_KEY}`;
    }

}