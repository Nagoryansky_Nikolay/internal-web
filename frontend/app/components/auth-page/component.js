import AuthPageController from './controller';

export const AuthPageComponent = {
    template: require('./auth-page-template.html'),
    controller: AuthPageController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        close: '&',
        dismiss: '&'
    },
};