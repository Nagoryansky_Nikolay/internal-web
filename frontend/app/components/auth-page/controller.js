export default class AuthPageController {
    constructor($scope, $http, $location, notifyService, $state, userService, authService) {
        'ngInject';
        this.$state = $state;
        this.$scope = $scope;
        this.$http = $http;
        this.userService = userService;
        this.authService = authService;
        this.$location = $location;
        this.notifyService = notifyService;
    }

    $onInit() {
        this.token = this.$location.search().token;
        if (this.token) {
            this.authService.setToken(this.token);
            this.$scope.$emit('changeToken',  this.token);
        }

        this.userService.getCurrentUser()
            .then((response) => {
                this.authService.setUser(response.data);
                this.$state.go('user.friends', {userId: response.data.id});
            })
            .catch((response) => {
                this.authService.logout();
                this.notifyService.error(response.data.message);
            });
    }
}

