export default class UsersListController {
    constructor($scope,userService, $location, listItemValues) {
        'ngInject';
        this.$scope = $scope;
        this.$location = $location;
        this.listItemValues = listItemValues;
        this.userService = userService;

        this.$scope.maxSize = 5;
        this.$scope.ITEMS_LIMIT = 8;
        this.$scope.totalItems = Number.MAX_SAFE_INTEGER;
        this.$scope.currentPage = this.$location.search().page || 1;
        this.$scope.currentItemsCount = this.$scope.ITEMS_LIMIT;

        this.$scope.ageList = this.listItemValues.getAgeList();
        this.$scope.genders = this.listItemValues.getGenderList();
        this.$scope.gender  = this.$location.search().gender || null;
        this.$scope.min_age = this.$location.search().min_age || null;
        this.$scope.max_age = this.$location.search().max_age || null;
        this.$scope.name= this.$location.search().name || null;

        this.$scope.conditions = {
            isShowedAgeList: false,
            isShowedGenderList: false,
            isSaving: false,
        };
    }

    getCurrentItemsCount(totalItems, page, limit ){
        let count = page * limit;
        if (totalItems <= count) return totalItems;
        else if(totalItems < limit) return totalItems % limit;
        return count;
    }

    $onInit() {
        this.$scope.age = this.getSelectedAge(this.$scope.min_age);
        this.userService.getUsers(
            this.$scope.currentPage-1,
            this.$scope.ITEMS_LIMIT,
            this.$scope.name,
            this.$scope.gender,
            this.$scope.min_age,
            this.$scope.max_age
        )
            .then((res) => {
                this.$scope.users = res.data.users;
                this.$scope.totalItems = res.data.totalCount;
                this.$scope.currentItemsCount = this.getCurrentItemsCount(
                    this.$scope.totalItems,
                    this.$scope.currentPage,
                    this.$scope.ITEMS_LIMIT
                )
            })
            .catch((err) => {
                this.notifyService.error(err.data.message);
            });

        this.$scope.$on('$locationChangeStart', () => {
            if (!this.$scope.name) this.$location.search('name', null);
            this.$scope.currentPage = this.$location.search().page || 1;
            this.userService.getUsers(
                this.$scope.currentPage-1,
                this.$scope.ITEMS_LIMIT,
                this.$location.search().name,
                this.$location.search().gender,
                this.$location.search().min_age,
                this.$location.search().max_age
            )
                .then((res) => {
                    this.$scope.users = res.data.users;
                    this.$scope.totalItems = res.data.totalCount;
                    this.$scope.currentItemsCount = this.getCurrentItemsCount(
                        this.$scope.totalItems,
                        this.$scope.currentPage,
                        this.$scope.ITEMS_LIMIT
                    )
                })
                .catch((err) => {
                    this.notifyService.error(err.data.message);
                });
        });

        this.$scope.searchValidationOptions = {
            rules:{
                searchString: {
                    minlength: 3,
                    regex: /^[a-zA-Z]{3,}\s*[a-zA-Z]*$/,
                },
            },
            messages: {
                searchString: {
                    regex: 'Allowed only valid lastname or firstname',
                },
            }
        };
    }

    setPage() {
        this.$location.search('page', this.$scope.currentPage);
    };

    setSearchName(form){
        this.$scope.conditions.isShowedAgeList = false;
        this.$scope.conditions.isShowedGenderList = false;
        if (!form.validate() || !this.$scope.name) {
            this.$location.search('name', null);
            return;
        }
        this.$location.search('name', this.$scope.name);
    }

    showGenderList(){
        this.$scope.conditions.isShowedGenderList = !this.$scope.conditions.isShowedGenderList;
        this.$scope.conditions.isShowedAgeList = false;
    }

    showAgeList(){
        this.$scope.conditions.isShowedAgeList = !this.$scope.conditions.isShowedAgeList;
        this.$scope.conditions.isShowedGenderList = false;

    }

    setGender(gender){
        this.$scope.gender = gender.name;
        this.$scope.genders.forEach(function(item, i, arr) { item.condition = false;});
        this.$scope.genders[gender.index].condition = true;
        this.$location.search('gender', this.$scope.gender);
    }

    setAge(age){
        this.$scope.age = age.name;
        this.$scope.ageList.forEach(function(item, i, arr) { item.condition = false;});
        this.$scope.ageList[age.index].condition = true;
        this.$location.search('min_age', age.min);
        this.$location.search('max_age', age.max);
    }

    getSelectedAge(min_age){
        let age = [];
        this.$scope.ageList.forEach(function(item, i, arr){
            if (item.min == min_age) age = item;});
        return age.name;
    }

    hideLists(){
        this.$scope.conditions.isShowedGenderList = false;
        this.$scope.conditions.isShowedAgeList = false;
    }
}