import UsersListController from './controller';

export const UsersListComponent = {
    template: require('./users-list-template.html'),
    controller: UsersListController,
    restrict: 'E',
    bindings: {
    },
};