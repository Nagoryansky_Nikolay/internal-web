export default class  UserDetailPageController{
    constructor($scope, userService, authService, $state, $stateParams, friendState, chatService) {
        'ngInject';
        this.$scope = $scope;
        this.authService = authService;
        this.userService = userService
        this.chatService = chatService;
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.friendState = friendState;

        this.$scope.userId = this.$stateParams.userId;
        this.$scope.isOwnerPage = this.$scope.userId == this.authService.user.id || this.$scope.userId === 'me';
        this.$scope.status = null;
        this.$scope.user = {};

        this.$scope.states = {
            TO_ADD: false,
            TO_ACCEPT: false,
            TO_REMOVE: false
        };
        this.$scope.FRIEND_STATUS = 0;
    }

    $onInit() {
        this.$scope.$watch('status', this._changeButtonsState.bind(this));
        this.userService.getUserById(this.$scope.userId)
            .then((response) => {
                this.$scope.user = response.data;
                this.$scope.status = this.$scope.user.friendshipStatus;
                this.$scope.user.avatar = this.$scope.user.avatar || this.$scope.DEFAULT_AVATAR;
                this.$scope.coverStyle = {
                    'background': `rgba(46, 31, 93, 1) url(${this.$scope.user.cover}) center no-repeat`,
                    'background-size': 'cover'
                };
            })
            .catch((response) => {
                this.notifyService.error(response.data.message);
            });
    };

    _changeButtonsState(){
        this.$scope.states = this.friendState.getFriendState(this.$scope.status, this.$scope.user.id);
    }

    editProfile(){
        this.$state.go('edit-profile');
    };

    addFriend(user){
        this.userService.addFriend(user.id)
            .then((response) => {
                this.$scope.status = response.data.user.friendshipStatus;
            })
            .catch((response) => {
                this.notifyService.error(response.data.message);
            });
    }

    acceptFriend(user){
        this.userService.acceptFriend(user.id)
            .then((response) => {
                this.$scope.status = response.data.user.friendshipStatus;
            })
            .catch((response) => {
                this.notifyService.error(response.data.message);
            });
    }

    removeFriend(user){
        this.userService.removeFriend(user.id)
            .then((response) => {
                this.$scope.status = response.data.user.friendshipStatus;
            })
            .catch((response) => {
                this.notifyService.error(response.data.message);
            });
    }

    goToChat(){
        let limit = 1;
        let offset = 0;
        this.chatService.getAllChats(offset, limit,  this.$scope.userId)
            .then(res => {
                this.$state.go('chats.detail', {chatId: res.data.chats[0].id})
            });
    }
}



