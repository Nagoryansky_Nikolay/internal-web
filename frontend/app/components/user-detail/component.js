import UserDetailPageController from './controller';

export const UserDetailPageComponent = {
    template: require('./user-detail-template.html'),
    controller: UserDetailPageController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
    },
};