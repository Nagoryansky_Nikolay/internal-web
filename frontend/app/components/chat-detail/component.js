import ChatDetailController from './controller';

export const ChatDetailComponent = {
    template: require('./chat-detail-template.html'),
    controller: ChatDetailController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
    },
};