export default class ChatDetailController{
    constructor($scope, $stateParams, $state, $timeout, chatService, notifyService, ioService, authService) {
        'ngInject';
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.chatService = chatService;
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.notifyService = notifyService;
        this.ioService = ioService;
        this.authService = authService;
        this.$scope.messageText = '';
        this.$scope.messages = [];
        this.$scope.targetUser = {};
        this.$scope.LIMIT = 10;
        this.$scope.offset = 0;
        this.$scope.chatId = this.$stateParams.chatId;
        this.$scope.status = {
            loading: false,
            loaded: false,
            typing: false,
            finishScroll: false
        };
    }

    $onInit() {

        this.loadMore();

        this.ioService.on('message', (data) => {
            if (this.$state.is('chats.detail') && this.$stateParams.chatId == data.chatId){
                this.$scope.messages.push(data);
                this.scrollToBottom(".chat-detail-message-container");
            }
        });

        this.ioService.on('typingMessage', () => {
            this.$scope.status.typing = true;
            this.$timeout(() => {this.$scope.status.typing = false;}, 2000);
        });

        this.ioService.on('err', (data) => {
            this.notifyService.error(data.message);
        });
    };


    loadMore() {
        if (!this.$scope.status.loading) {
            this.$scope.status.loading = true;
            this.chatService.getAllMessages(this.$scope.chatId, this.$scope.offset, this.$scope.LIMIT )
                .then((res) => {
                    this.$scope.messages = res.data.messages.reverse().concat(this.$scope.messages);
                    this.$scope.targetUser = res.data.user;
                    this.$scope.offset += this.$scope.LIMIT;
                    this.$scope.status.finishScroll =  res.data.messages.length < this.$scope.LIMIT;
                    this.$scope.status.loaded = ( this.$scope.messages.length > 0);
                    this.$scope.status.loading = false;
                })
                .catch((err) => {
                    this.$scope.status.loading = false;
            });
        }

    };

    goToChatsList(){
        this.$state.go('chats.list')
    };

    changeInput(){
        this.ioService.emit('typingMessage',{chatId: this.$scope.chatId});
    }

    sendMessage(){
        let tmp = this.$scope.messageText;
        this.$scope.messageText = '';
        this.ioService.emit('message', {
            text: tmp,
            chatId: this.$scope.chatId,
        })
    }

    scrollToBottom(selector){
        this.$timeout(() => {
            const elem = angular.element(document.querySelector(selector))[0];
            elem.scrollTop = elem.scrollHeight;
        }, 50);
    }

}



