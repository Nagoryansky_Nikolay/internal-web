export default class  FriendsListController{
    constructor($scope, $location, userService) {
        'ngInject';
        this.$scope = $scope;
        this.userService = userService;
        this.$location = $location;
        this.$scope.friends = {};

        this.$scope.maxSize = 5;
        this.$scope.ITEMS_LIMIT = 4;
        this.$scope.totalItems = Number.MAX_SAFE_INTEGER;
        this.$scope.currentPage = this.$location.search().page || 1;
        this.$scope.currentItemsCount = this.$scope.ITEMS_LIMIT;

        this.$scope.INPUT_DELAY = 700;
    }

    $onInit() {
        this.userService.getUserFriends(
            this.userId,
            this.$scope.currentPage-1,
            this.$scope.ITEMS_LIMIT,
            this.$scope.name,
        )
            .then((res) => {
                this.$scope.friends = res.data.users;
                this.$scope.totalItems = res.data.totalCount;
                this.$scope.currentItemsCount = this.getCurrentItemsCount(
                    this.$scope.totalItems,
                    this.$scope.currentPage,
                    this.$scope.ITEMS_LIMIT
                )
            })
            .catch((err) => {
                this.notifyService.error(err.data.message);
            });

        this.$scope.$on('$locationChangeStart', () => {
            if (!this.$scope.name) this.$location.search('name', null);
            this.$scope.currentPage = this.$location.search().page || 1;
            this.userService.getUserFriends(
                this.userId,
                this.$scope.currentPage-1,
                this.$scope.ITEMS_LIMIT,
                this.$location.search().name,
            )
                .then((res) => {
                    this.$scope.friends = res.data.users;
                    this.$scope.totalItems = res.data.totalCount;
                    this.$scope.currentItemsCount = this.getCurrentItemsCount(
                        this.$scope.totalItems,
                        this.$scope.currentPage,
                        this.$scope.ITEMS_LIMIT
                    )
                })
                .catch((err) => {
                    this.notifyService.error(err.data.message);
                });
        });

        this.$scope.searchValidationOptions = {
            rules:{
                searchString: {
                    minlength: 3,
                    regex: /^[a-zA-Z]{3,}\s*[a-zA-Z]*$/,
                },
            },
            messages: {
                searchString: {
                    regex: 'Allowed only valid lastname or firstname',
                },
            }
        };
    };

    setPage(){
        this.$location.search('page', this.$scope.currentPage);
    };

    setSearchName(form){
        if (!form.validate() || !this.$scope.name) {
            this.$location.search('name', null);
            return;
        }
        this.$location.search('name', this.$scope.name);
    }

    getCurrentItemsCount(totalItems, page, limit ){
        let count = page * limit;
        if (totalItems <= count) {return totalItems}
        else if(totalItems < limit) {return totalItems % limit};
        return count;
    }

}



