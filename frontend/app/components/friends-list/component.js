import FriendsListController from './controller';

export const FriendsListComponent = {
    template: require('./friends-list-template.html'),
    controller: FriendsListController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        userId: '<'
    },
};