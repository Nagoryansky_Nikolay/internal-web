import LoginPopupController from './controller';

export const LoginPopupComponent = {
    template: require('./login-popup-template.html'),
    controller: LoginPopupController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        close: '&',
        dismiss: '&'
    },
};

