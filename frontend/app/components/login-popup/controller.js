export default class LoginPopupController {
    constructor($scope, $http, ngNotify, $state, authService, notifyService) {
        'ngInject';
        this.$scope = $scope;
        this.$scope.isLoginOpened = true;
        this.$http = $http;
        this.authService = authService;
        this.notifyService = notifyService;
        this.ngNotify = ngNotify;
        this.$state = $state;
    }

    $onInit () {
        this.$scope.user = {};
        this.$scope.isSaving = false;
        this.$scope.validationOptions = {
            rules:{
                email: {
                    email: true,
                    required: true
                },
                password: {
                    minlength: 6,
                    regex: '^[^\\s]+$',
                    required: true
                }
            },
            messages: {
                password: {
                    regex: 'Allowed any character except space',
                }
            }
        }
    };

    login(form) {
        if (form.validate()) {
            this.$scope.isSaving = true;
            this.authService.login(this.$scope.user)
                .then((response) => {
                    this.authService.setToken(response.data.session.accessToken);
                    this.authService.setUser(response.data.user);
                    this.$scope.$emit('changeToken',  response.data.session.accessToken);
                    this.$state.go('user.friends', {userId: response.data.user.id});
                    this.$scope.isSaving = false;
                    this.close();
                })
                .catch((response) => {
                    this.notifyService.error(response.data.message);
                    this.$scope.isSaving = false;
                });
        }
    };

    goRequestRestorePage(){
        this.close();
        this.$state.go('request-restore-password');
    }

}
