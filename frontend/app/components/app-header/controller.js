export default class AppHeaderController {
    constructor($uibModal,$scope, $state, authService) {
        'ngInject';

        this.$uibModal = $uibModal;
        this.$state = $state;
        this.$scope = $scope;
        this.authService = authService;
        this.$scope.isNavCollapsed = true;
        this.$scope.isCollapsed = false;
        this.$scope.isCollapsedHorizontal = false;
    }

    $onInit() {

    }

    isAuth(){
        return this.authService.isAuth()
    }

    logout(){
        this.authService.logout();
        this.$state.go('landing');
    }


    openRegisterPopup() {
        this.$uibModal.open({
            component: 'registerPopup',
            windowClass: 'register-modal-window',

            }).result
            .then(function(res) {
                console.log('$close called');
            })
            .catch(function(res) {
                console.log('$dismiss called');
            });
        }

    openLoginPopup() {
        this.$uibModal.open({
            component: 'loginPopup',
            windowClass: 'login-modal-window',

        }).result
            .then(function(res) {
                console.log('$close called');
            })
            .catch(function(res) {
                console.log('$dismiss called');
            });
    }



}
