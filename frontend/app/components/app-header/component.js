import AppHeaderController from './controller';

export const AppHeaderComponent = {
    template: require('./app-header-template.html'),
    controller: AppHeaderController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
    }

};