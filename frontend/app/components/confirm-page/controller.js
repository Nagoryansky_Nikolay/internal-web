export default class ConfirmPageController {
    constructor($scope, $http, $location, notifyService, $state, authService) {
        'ngInject';
        this.$state = $state;
        this.$http = $http;
        this.$location = $location;
        this.authService = authService;
        this.notifyService = notifyService;
    }

    $onInit() {
        this.token = this.$location.search().token;
        if (this.token) {
            this.authService.confirmToken(this.token)
                .then((response) => {
                    this.authService.setToken(response.data.session.accessToken);
                    this.authService.setUser(response.data.user);
                    this.$scope.$emit('changeToken',  response.data.session.accessToken);
                    this.$state.go('user.friends', {userId: response.data.id});
                    this.notifyService.notify(response.data.message);
                })
                .catch((response) => {
                    this.$state.go('landing');
                    this.notifyService.error(response.data.message);
                });

        }
    }
}