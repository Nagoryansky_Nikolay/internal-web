import ConfirmPageController from './controller';

export const ConfirmPageComponent = {
    template: require('./confirm-page-template.html'),
    controller: ConfirmPageController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        close: '&',
        dismiss: '&'
    },
};