import RegisterPopupController from './controller';

export const RegisterPopupComponent = {
    template: require('./register-popup-template.html'),
    controller: RegisterPopupController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        close: '&',
        dismiss: '&'
    },
};

