export default class RegisterPopupController {
    constructor($scope, $http, notifyService, authService) {
        'ngInject';
        this.$scope = $scope;
        this.$http = $http;
        this.notifyService = notifyService;
        this.authService = authService;
    }

    $onInit () {
        this.$scope.user = {};
        this.$scope.isSaving = false;
        this.$scope.validationOptions = {
            rules:{
                username: {
                    minlength: 3,
                    regex: '^[a-z0-9-]+$',
                    required: true
                },
                email: {
                    email: true,
                    required: true
                },
                password: {
                    minlength: 6,
                    regex: '^[^\\s]+$',
                    required: true
                },
                confirm_password: {
                    minlength: 6,
                    regex: '^[^\\s]+$',
                    required: true,
                    equalTo: "#password"
                },
                firstname: {
                    minlength: 3,
                    regex: '^[a-zA-Z]+$',
                    required: true
                },
                lastname: {
                    minlength: 3,
                    regex: '^[a-zA-Z]+$',
                    required: true
                }
            },
            messages: {
                username: {
                    regex: 'Allowed only alpha-numeric characters and dashes on lower case',
                },
                password: {
                    regex: 'Allowed any character except space',
                },
                confirm_password: {
                    equalTo: 'Must be the same as “password” field',
                }
            }
        }
    };

    register (form) {
        if( form.validate() ) {
            this.$scope.isSaving= true;
            this.authService.register(this.$scope.user)
                .then((response) => {
                    this.notifyService.notify(response.data.message);
                    this.$scope.isSaving = false;
                    this.close();
                }).catch((response) => {
                    this.notifyService.error(response.data.message);
                });
        };

    };

}
