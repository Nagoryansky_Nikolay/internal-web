export default class ChatsListController {
    constructor($scope, $state, chatService, notifyService) {
        'ngInject';
        this.$scope = $scope;
        this.chatService = chatService;
        this.$scope.chats = [];
        this.$state = $state;
        this.notifyService = notifyService;
        this.$scope.LIMIT = 10;
        this.$scope.offset = 0;
        this.$scope.isBusy = false
    }

    $onInit() {
    };

    getNextChats(){
        if (this.$scope.isBusy)
            return;
        this.$scope.isBusy = true;
        this.chatService.getAllChats(this.$scope.offset, this.$scope.LIMIT)
            .then((res) => {
                this.$scope.offset += this.$scope.LIMIT;
                this.$scope.chats = this.$scope.chats.concat(res.data.chats);
                this.$scope.isBusy =  res.data.chats.length < this.$scope.LIMIT;
            })
            .catch((err) => {
                this.notifyService.error(err.data.message);
            });
    }


}