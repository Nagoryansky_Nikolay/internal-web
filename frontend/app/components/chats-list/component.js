import ChatsListController from './controller';

export const ChatsListComponent = {
    template: require('./chats-list-template.html'),
    controller: ChatsListController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
    },
};