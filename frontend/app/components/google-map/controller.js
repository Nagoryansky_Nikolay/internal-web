export default class GoogleMapController {
    constructor($scope, $http, ngNotify, $state, authService, notifyService,$window, $timeout) {
        'ngInject';
        this.$window = $window;
        this.$timeout = $timeout;
        this.$scope = $scope;
        this.$http = $http;
        this.authService = authService;
        this.notifyService = notifyService;
        this.ngNotify = ngNotify;
        this.$state = $state;
        this.$scope.defaultMarkerImg = 'https://s3-eu-west-1.amazonaws.com/internal-web-bucket/common/ic_location-2x.png';

    }

    $onInit () {
        this.$scope.mapStyle = {
            'height': this.$window.innerHeight*0.7
        };
        const elem = document.getElementById('google-map--map-body');
        const options = {
            center: this.resolve.center,
            scrollwheel: true,
            zoom: 13
        };

        this.initMap(elem, options)
            .then((map) =>{
                let m = new google.maps.Marker({
                    position: new google.maps.LatLng(options.center.lat, options.center.lng),
                    map: map ,
                    icon: this.$scope.defaultMarkerImg
                });
                this.$timeout(()=>{
                    this.resolve.events.forEach((item) => {
                        this.setMarkers(map, item)
                    });
                },200);

            }
        );

    }

    initMap(elem, options){
        return new Promise((resolve, reject) => {
            this.$timeout(() => {
                let map = new google.maps.Map(elem, options);
                resolve(map);
            }, 50);
        });
    }

    setMarkers(map, event) {
        const image = {
            url: event.user.avatar,
            size: new google.maps.Size(60, 60),
            scaledSize: new google.maps.Size(60, 60),
            origin: new google.maps.Point(0, 0),
        };

        let marker = new google.maps.Marker({
            position: new google.maps.LatLng(event.lat, event.lng),
            map: map ,
            icon: image
        });
    }




}


