import GoogleMapController from './controller';

export const GoogleMapComponent = {
    template: require('./google-map-template.html'),
    controller: GoogleMapController,
    controllerAs: '$ctrl',
    restrict: 'E',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    },
};

