export function whenScrolledDirective($timeout) {
    return function(scope, elm, attr) {
        var raw = elm[0];
        elm.bind('scroll', function() {
            if (raw.scrollTop <= 100  && !scope.status.finishScroll) {
                var sh = raw.scrollHeight;
                scope.$apply(attr.whenScrolled);
                $timeout(function(){
                    raw.scrollTop = raw.scrollHeight - sh;
                }, 1000);
            }
        });
    };
}
