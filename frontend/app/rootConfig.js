export default function($stateProvider, $urlRouterProvider, $locationProvider, $validatorProvider, $httpProvider) {

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

    const headers = {
        'Content-Type': 'application/json',
        'charset': 'utf-8'
    };
    $httpProvider.defaults.headers.post = headers;
    $httpProvider.defaults.headers.delete = headers;
    $httpProvider.defaults.headers.put = headers;
    $httpProvider.defaults.headers.patch = headers;


    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('landing', {
            url: '/',
            template: '<landing-page></landing-page>'
        })
        .state('confirm-mail', {
            url: '/confirm-mail',
            template: '<confirm-page></confirm-page>'
        })
        .state('request-restore-password', {
            url: '/request-restore-password',
            template: '<request-restore-password-page></request-restore-password-page>'
        })
        .state('restore-password', {
            url: '/restore-password',
            template: '<landing-page></landing-page><restore-password-page></restore-password-page>'
        })
        .state('auth', {
            url: '/auth',
            template: '<auth-page></auth-page>',
        })
        .state('edit-profile', {
            url: '/edit-profile',
            template: '<edit-profile-page></edit-profile-page>',
            resolve: {
                isValidToken: ['authService', function(authService) {
                    return authService.checkToken();
                }]
            }
        })
        .state('feed', {
            url: '/feed',
            template: '<feed-page></feed-page>',
            resolve: {
                isValidToken: ['authService', function(authService) {
                    return authService.checkToken();
                }]
            }
        })
        .state('search', {
            url: '/search',
            template: '<users-list></users-list>',
            resolve: {
                isValidToken: ['authService', function(authService) {
                    return authService.checkToken();
                }]
            }
        })
        .state('user', {
            url: '/:userId',
            absract: true,
            template: '<user-detail></user-detail><ui-view></ui-view>',
            resolve: {
                isValidToken: ['authService', function(authService) {
                    return authService.checkToken();
                }],
            }
        })
        .state('user.events', {
            url: '/events',
            component: 'eventsList'
        })
        .state('user.friends', {
            url: '/friends',
            component: 'friendsList',
            resolve: {
                userId: ['$stateParams', function ( $stateParams) {
                    return $stateParams.userId;
                }]
            }
        })
        .state('chats', {
            url: '/chats',
            absract: true,
            template: '<ui-view></ui-view>',
            resolve: {
                isValidToken: ['authService', function(authService) {
                    return authService.checkToken();
                }]
            }
        })
        .state('chats.list', {
            url: '/list',
            template: '<chats-list></chats-list>'
        })
        .state('chats.detail', {
            url: '/:chatId',
            template: '<chat-detail></chat-detail>'
        });

    $httpProvider.interceptors.push(['$q', '$injector', function ($q, $injector) {
        return {
            'responseError': function(error) {
                if (error.status === 401 || error.status === 410) {
                    $injector.get('$state').go('landing');
                    $injector.get('notifyService').notify('Please login or register to view this page!');
                    $injector.get('authService').logout();
                }
                return $q.reject(error);
            },
        };
    }]);

    $validatorProvider.addMethod(
        'regex',
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        'Please check your input.'
    );

    $validatorProvider.addMethod(
        'isPasswordEmpty',
        function (value, element, params) {
            let value2 = angular.element( document.querySelector( params ))[0].value;
            return !(value2 && !element.value);
        },
        'Password is required!'
    );

}

