export default class RootController {
    constructor($scope, $rootScope, ioService, authService, IO_SERVER, notifyService, $stateParams, $state) {
        'ngInject';
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.ioService = ioService;
        this.authService = authService;
        this.notifyService = notifyService;
        this.$stateParams = $stateParams;
        this.$state = $state;
        this.IO_SERVER = IO_SERVER;
    }

    $onInit() {
        if ( this.authService.token ){
            this.ioService.connect(this.IO_SERVER, this.authService.token );
            this.ioService.on('message', (data) => {
                if (data.senderId !==  this.authService.user.id && !(this.$state.is('chats.detail') && this.$stateParams.chatId == data.chatId) ){
                    this.notifyService.notify(`${data.user.firstname} ${data.user.lastname} has sent you a private message!`);
                }
            });
        }

        this.$rootScope.$on('changeToken',
            (event, data)=>{
                this.ioService.connect(this.IO_SERVER, data);
                this.ioService.on('message', (data) => {
                    if (data.senderId !==  this.authService.user.id && !(this.$state.is('chats.detail') && this.$stateParams.chatId == data.chatId) ){
                        this.notifyService.notify(`${data.user.firstname} ${data.user.lastname} has sent you a private message!`);
                    }
                });
            });




    }

}