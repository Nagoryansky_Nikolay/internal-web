import ng from 'angular';

import 'bootstrap/dist/css/bootstrap.css';
import '../../node_modules/angular-carousel/dist/angular-carousel.css';
import '../../node_modules/ng-notify/dist/ng-notify.min.css';
import '../../node_modules/ng-img-crop/compile/unminified/ng-img-crop.css';
import './components/app-header/app-header.css';
import './components/landing-page/landing-page.css';
import './components/register-popup/register-popup.css';
import './components/login-popup/login-popup.css';
import './components/feed-page/feed-page.css';
import './components/users-list/users-list.css';
import './components/confirm-page/confirm-page.css';
import './components/edit-profile-page/edit-profile-page.css';
import './components/restore-password/restore-password.css';
import './components/request-restore-password/request-restore-password.css';
import './components/user-item/user-item.css';
import './components/user-detail/user-detail.css';
import './components/chat-item/chat-item.css';
import './components/chat-detail/chat-detail.css';
import './components/chats-list/chats-list.css';
import './components/message/message.css';
import './components/event-item/event-item.css';
import './components/events-list/events-list.css';
import './components/friends-list/friends-list.css';
import './components/google-map/google-map.css'


import 'angular-ui-bootstrap';
import '../../node_modules/angular-form-validate/dist/angular-validate.min.js';
import '../../node_modules/ng-notify/dist/ng-notify.min.js';
import '../../node_modules/ng-img-crop/compile/unminified/ng-img-crop.js';
import '../../node_modules/ng-infinite-scroll/build/ng-infinite-scroll'
import ngFileUpload from 'ng-file-upload';
import ngRoute from 'angular-ui-router';
import 'angular-moment';
import '../../node_modules/angular-masonry/angular-masonry.js';
import '../../node_modules/angular-deckgrid/angular-deckgrid.js';

import '../../node_modules/angular-carousel/dist/angular-carousel.js';
import '../../node_modules/angular-touch/angular-touch.js';

import RootController from './rootController';
import RootConfig from './rootConfig';

import { AppHeaderComponent } from './components/app-header/component';
import { LandingPageComponent } from './components/landing-page/component';
import { RegisterPopupComponent } from './components/register-popup/component';
import { LoginPopupComponent } from './components/login-popup/component';
import { FeedPageComponent } from './components/feed-page/component';
import { UsersListComponent } from './components/users-list/component';
import { ConfirmPageComponent } from './components/confirm-page/component';
import { EditProfilePageComponent } from './components/edit-profile-page/component';
import { RestorePasswordComponent } from './components/restore-password/component';
import { RequestRestorePasswordComponent } from './components/request-restore-password/component';
import { AuthPageComponent } from './components/auth-page/component';
import { UserItemComponent } from './components/user-item/component';
import { UserDetailPageComponent } from './components/user-detail/component';
import { ChatItemComponent } from './components/chat-item/component';
import { ChatDetailComponent } from './components/chat-detail/component';
import { ChatsListComponent } from './components/chats-list/component';
import { MessageComponent } from './components/message/component';
import { EventsListComponent } from './components/events-list/component';
import { EventItemComponent } from './components/event-item/component';
import { FriendsListComponent } from './components/friends-list/component';
import { GoogleMapComponent } from './components/google-map/component';


import { NotificationService } from './services/notification';
import { AuthService } from './services/authService';
import { UserService } from './services/userService';
import { ListItemValues } from './services/listItemValues';
import { FriendStateService } from './services/friendState';
import { ChatService } from './services/chatService';
import { IOService } from './services/ioService';
import { EventService } from './services/eventService';

import { whenScrolledDirective } from './directives/whenScrolled';
import { scrollBottomOnDirective } from './directives/scrollBottomOn';


ng.module('app', ['ui.bootstrap', ngRoute, 'ngValidate', 'ngNotify', 'angularMoment', ngFileUpload, 'ngImgCrop', 'infinite-scroll', 'akoenig.deckgrid','angular-carousel'])
    .config(RootConfig)
    .controller('RootController', RootController)
    .component('appHeader', AppHeaderComponent)
    .component('landingPage', LandingPageComponent)
    .component('registerPopup', RegisterPopupComponent)
    .component('loginPopup', LoginPopupComponent)
    .component('feedPage', FeedPageComponent)
    .component('usersList', UsersListComponent)
    .component('confirmPage', ConfirmPageComponent)
    .component('editProfilePage', EditProfilePageComponent)
    .component('restorePasswordPage', RestorePasswordComponent)
    .component('requestRestorePasswordPage', RequestRestorePasswordComponent)
    .component('authPage', AuthPageComponent)
    .component('userItem', UserItemComponent)
    .component('userDetail', UserDetailPageComponent)
    .component('chatItem', ChatItemComponent)
    .component('chatDetail', ChatDetailComponent)
    .component('chatsList', ChatsListComponent)
    .component('message', MessageComponent)
    .component('eventsList', EventsListComponent)
    .component('eventItem', EventItemComponent)
    .component('friendsList', FriendsListComponent)
    .component('googleMap', GoogleMapComponent)
    .service('notifyService', NotificationService)
    .service('authService', AuthService)
    .service('userService', UserService)
    .service('friendState', FriendStateService)
    .service('listItemValues', ListItemValues)
    .service('chatService', ChatService)
    .service('ioService', IOService)
    .service('eventService', EventService)
    .constant('DEFAULT_AVATAR', 'https://s3-eu-west-1.amazonaws.com/internal-web-bucket/avatars/default-avatar.png')
    .constant('DEFAULT_COVER', 'https://s3-eu-west-1.amazonaws.com/internal-web-bucket/covers/default-cover.jpg')
    .constant('BASE_URL', window.location.host)
    .constant('IO_SERVER', `http://${window.location.hostname}:5001`)
    .directive('whenScrolled', whenScrolledDirective)
    .directive('scrollBottomOn', scrollBottomOnDirective);

