# http://34.251.247.58:5000/ #
Last updated at 1.03.2017 19:25,
[Commit](https://bitbucket.org/Nagoryansky_Nikolay/internal-web/commits/54d24ce55c3b8a1a0cbdcdabc36c8024085a7f94)
## Test users' pasword is *111111* and emails are: ##
* test@web.com
* bunxxegu@yahsssoo.com
* bunegu@yahsssoo.com


Install project
--------------
-Install npm packager

```sh
$ npm install
```

Config project
--------------

```sh
cp config.json.dist config.json
```

- For dev

```sh
cp -r env.dist dev
```



- For prod
```sh
cp -r env.dist prod
```


- For test
```sh
cp -r env.dist test
```

Run Migrations
--------------

- For production
```sh
$ node_modules/.bin/sequelize db:migrate --env prod
```


- For dev
```sh
$ node_modules/.bin/sequelize db:migrate --env dev
```


- For undo
```sh
$ node_modules/.bin/sequelize db:migrate:undo --env dev
```