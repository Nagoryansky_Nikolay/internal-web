'use strict';

let db = require('./../config.json').dev;

module.exports = {
    host: process.env.DB_HOST || db.host,
    port: parseInt(process.env.DB_PORT) || db.port,
    dbname: process.env.DB_NAME || db.database,
    username: process.env.DB_USER || db.username,
    password: process.env.DB_PASSWORD || db.password,
    dialect: db.dialect,
    charset: 'utf8',
    connectionRetryCount: 5,
    maxConnections: 10,
    delayBeforeReconnect: 3000,
    showErrors: true
};
