'use strict';

const db = require('./db');
const redis = require('./redis');

const baseUrl = process.env.HOST ? 'http://' + process.env.HOST + ':' + process.env.PORT : 'http://localhost:9999';

let defaults = {
    server: {
        port: parseInt(process.env.PORT) || 9999,
        host: process.env.HOST || 'localhost',
        baseUrl: baseUrl
    },
    db: db,
    redis: redis,

    chat : {
        db: db,
        mongoURI: 'mongodb://[username:password@]host1[:port1][/[database][?options]]'
    },

    jwt: {
        user:{
            jwtKey: "some-string",
            jwtLifeTime: 1000 * 60 * 60 * 24,
        },
        confirmMail:{
            jwtKey: "some-string",
            jwtLifeTime: 1000 * 60 * 60 * 24,
        },
        restorePassword:{
            jwtKey: "some-string",
            jwtLifeTime: 1000 * 60 * 60 * 24,
        }

    },
    mail: {
        service: 'your-mail-service',
        auth: {
            user: 'email-address',
            pass: 'email-password'
        }
    },
    sn:{
        facebookAuth : {
            clientID      : 'your-secret-clientID-here',
            clientSecret  : 'your-client-secret-here',
            callbackURL   : '/auth/facebook/callback'
        },
        twitterAuth : {
            consumerKey       : 'your-consumer-key-here',
            consumerSecret    : 'your-client-secret-here',
            callbackURL       : '/auth/twitter/callback'
        },
        googleAuth : {
            clientID      : 'your-secret-clientID-here',
            clientSecret  : 'your-client-secret-here',
            callbackURL   : '/auth/google/callback'
        },
        instagramAuth:{
            clientID      : 'your-secret-clientID-here',
            clientSecret  : 'your-client-secret-here',
            callbackURL   : "/auth/instagram/callback"
        }
    },
    s3:{
        bucketUrl: 'https://s3-eu-west-1.amazonaws.com/bucketName/',
        AVATARS_PATH: 'avatars/',
        SM_AVATARS_PATH: 'sm_avatars/',
        COVERS_PATH: 'covers/',
        EVENTS_PATH: 'events/',
        region: 'eu-west-1c',
        accessKeyId: '<your-accessKeyId>',
        secretAccessKey: '<your-secretAccessKey>',
        bucketName: 'bucketName'
    },
    defaultUserFields: {
        AVATAR: 'default-avatar.png',
        COVER: 'default-cover.jpg',
        AVATAR_URL: 'https://s3-eu-west-1.amazonaws.com/internal-web-bucket/avatars/default-avatar.png',
        COVER_URL: 'https://s3-eu-west-1.amazonaws.com/internal-web-bucket/covers/default-cover.jpg',
        FEMALE_GENDER : 'Female',
        MALE_GENDER: 'Male',
        FIRST_NAME: 'Unknown',
        LAST_NAME: 'Unknown',
        EMAIL_DOMAIN: '@example.com',
        PASSWORD: '*******'
    }
};

module.exports = defaults;
