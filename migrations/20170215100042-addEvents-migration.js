'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('events', {
          id: {
              type: Sequelize.INTEGER,
              primaryKey: true,
              autoIncrement: true
          },
          user_id: {
              type: Sequelize.INTEGER,
              references: {
                  model: 'users',
                  key: 'id'
              },
              onUpdate: 'cascade',
              onDelete: 'cascade'
          },
          description: {
              type: Sequelize.STRING,
              required: true
          },
          latitude: {
              type: Sequelize.FLOAT,
              allowNull: true
          },
          longitude: {
              type: Sequelize.FLOAT,
              allowNull: true
          },
          is_deleted : {
              type: Sequelize.BOOLEAN,
              defaultValue: false,
              allowNull: false
          },
          createdAt: {
              type: Sequelize.DATE
          },
          updatedAt: {
              type: Sequelize.DATE
          }
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('events');
  }
};
