'use strict';

module.exports = {
    up: function (queryInterface, Sequelize) {
        return queryInterface.createTable('users', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },

            user_name: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },

            email: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },

            password: {
                type: Sequelize.STRING,
                allowNull: false
            },

            first_name: {
                type: Sequelize.STRING,
                allowNull: true
            },

            last_name: {
                type: Sequelize.STRING,
                allowNull: true
            },

            followers_count: {
                type: Sequelize.INTEGER,
                defaultValue: 0,
            },

            avatar: {
                type: Sequelize.STRING,
                allowNull: true
            },

            cover: {
                type: Sequelize.STRING,
                allowNull: true
            },

            birthday: {
                type: Sequelize.DATEONLY,
                allowNull: true
            },

            gender: {
                type: Sequelize.BOOLEAN,
                defaultValue: null,
                allowNull: true
            },

            city: {
                type: Sequelize.STRING,
                allowNull: true
            },

            country: {
                type: Sequelize.STRING,
                allowNull: true
            },

            latitude: {
                type: Sequelize.FLOAT,
                allowNull: true
            },

            longitude: {
                type: Sequelize.FLOAT,
                allowNull: true
            },

            createdAt: {
                type: Sequelize.DATE
            },

            updatedAt: {
                type: Sequelize.DATE
            },

            is_confirm_email: {
                type: Sequelize.BOOLEAN,
                defaultValue: false,
                allowNull: true
            },

            facebook_id: {
                type: Sequelize.STRING,
                allowNull: true
            },

            twitter_id: {
                type: Sequelize.STRING,
                allowNull: true
            },

            google_id: {
                type: Sequelize.STRING,
                allowNull: true
            },

            instagram_id: {
                type: Sequelize.STRING,
                allowNull: true
            },
        });
    },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('users');
    }
};

