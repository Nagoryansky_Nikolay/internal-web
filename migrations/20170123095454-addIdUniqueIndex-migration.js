'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.addIndex('friendships', ['user_id', 'friend_id'],  {
          indexName: 'FriendIdUserIdIndex',
              indicesType: 'UNIQUE'
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.removeIndex('friendships', ['user_id', 'friend_id'],{
          indexName: 'FriendIdUserIdIndex',
      });
  }
};
