'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('eventPhotos', {
          id: {
              type: Sequelize.INTEGER,
              primaryKey: true,
              autoIncrement: true
          },

          event_id: {
              type: Sequelize.INTEGER,
              references: {
                  model: 'events',
                  key: 'id'
              },
              onUpdate: 'cascade',
              onDelete: 'cascade'
          },

          photo: {
              type: Sequelize.STRING,
              required: true
          },

          createdAt: {
              type: Sequelize.DATE
          },

          updatedAt: {
              type: Sequelize.DATE
          }
      });
  },

    down: function (queryInterface, Sequelize) {
        return queryInterface.dropTable('eventPhotos');
    }
};
