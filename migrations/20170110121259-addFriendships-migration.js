'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
      return queryInterface.createTable('friendships', {
          id: {
              type: Sequelize.INTEGER,
              primaryKey: true,
              autoIncrement: true
          },

          user_id: {
              type: Sequelize.INTEGER,
              references: {
                  model: 'users',
                  key: 'id'
              }
          },

          friend_id: {
              type: Sequelize.INTEGER,
              references: {
                  model: 'users',
                  key: 'id'
              }
          },

          status: {
              type: Sequelize.INTEGER,
              allowNull: false
          },

          createdAt: {
              type: Sequelize.DATE,
          },

          updatedAt: {
              type: Sequelize.DATE,
          }
      });
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('friendships');
  }
};
