'use strict';

const IOServer = require('./server/io/IOServer');
const config = require('./config');
const http = require('http');
const env = process.env.NODE_ENV || 'dev';
const cluster = require('cluster');
const sticky = require('socketio-sticky-session');
const numCPUs = require('os').cpus().length;

var options = {
    proxy: false, //activate layer 4 patching
    header: 'x-forwarded-for', //provide here your header containing the users ip
    num: numCPUs -1
};

if(cluster.isMaster){
    console.log("Master is now live at port " + (config.server.port + 1) + " with " + env + " config");
}

sticky(options, function() {

    // This code will be executed only in slave workers
    var server = http.createServer(serverStartCallback);

    new IOServer(server);

    return server;

}).listen(config.server.port + 1, function() {
    if(!cluster.isMaster){
        console.log('Cluster with pid ' + process.pid + ' started');
    }
});

function serverStartCallback(req, res) {
    res.writeHead(200,{ 'Content-Type': 'text/html' });
    res.end('<h1>Socket listener</h1>');
}

// const server = http.createServer(serverStartCallback);
//
// new IOServer(server);
//
// server.listen(config.server.port + 1, function () {
//     console.log('Instance launched at', config.server.port + 1, 'port');
// });
